export interface IpcResponderControllerResponse {
    code: string;
    data?: unknown;
    success?: boolean;
    error?: {
        message: string;
        nodeError?: unknown;
    }; 
}
