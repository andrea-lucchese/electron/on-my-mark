import { useContents } from '@/compositions';
import ipcRendererRequest from '@/ipc/ipcRendererRequest';

export const getTreeContents = async () => {
    const storage = useContents().getCurrentStorage.value;

    const response = await ipcRendererRequest({ 
        method: 'GET', 
        action: 'getDirContent',
        args: { storage }
    });

    return response && response.data;
}