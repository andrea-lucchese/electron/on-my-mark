import { useContents } from '@/compositions';
import ipcRendererRequest from '@/ipc/ipcRendererRequest';

/**
 * Make a request to delete an attachment.
 * @param relPath
 */
export const deleteAttachment = async (attachmentFileName = ''): Promise<string> => {
    const storage = useContents().getCurrentStorage.value;

    if (!attachmentFileName) return '';

    const response = await ipcRendererRequest({
        method: 'GET',
        action: 'deleteAttachment',
        args: { storage, attachmentFileName }
    });

    const data = response.data as Record<'deletedFile', string>;

    return data.deletedFile;
}