/**
 * Strips the extension from a file name.
 * @param {string} fileName
 * @returns {string}
 */
export const nameWithoutExtension = (fileName: string): string => {
    return fileName.replace(/\.[^/.]+$/, "");
}
