export interface CreatedFile {
    fileName: string;
    originalFileName: string;
    extension: string;
}
