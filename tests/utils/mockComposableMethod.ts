/**
 * Imports a composition file and mocks a specified function.
 * @param {string} methodName - The name of the composition function/file
 * @param {string} composableMethodName - The name of the method we want to mock
 * @param {(() => unknown)|undefined} mockImplementation - An implementation as jest mock
 */
export const mockComposableMethod = async (
        modulePath: string,
        methodName: string,
        composableMethodName: string,
        mockImplementation: (() => unknown)|undefined = undefined
    ) => {
    const composable = await import(modulePath);

    mockImplementation = mockImplementation || jest.fn();

    const composableResult = composable[methodName]();

    return jest.spyOn(composable, methodName).mockImplementation(() => {
        return {
            ...composableResult,
            [composableMethodName]: mockImplementation
        }
    })
}

/**
 * Example Implementation without above helper.
 * We want to mock useContents().removeMetaAttachment
 *
 * // 1. Import the useContents as a module.
 * import * as importUseContents from "@/compositions/useContents";
 *
 * // 2. Gets the result of the composition function.
 * const useContentsReturn = importUseContents.useContents();
 *
 * // 3. Spy the composition function of the imported module,
 * // return the composition function with our mocked implementation.
 * const useContents = jest.spyOn(importUseContents, 'useContents')
 *     .mockImplementation(() => {
 *         return { ...useContentsReturn, removeMetaAttachment  }
 *     });
 */
