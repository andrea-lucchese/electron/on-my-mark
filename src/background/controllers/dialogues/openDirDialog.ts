import { dialog } from 'electron';
import { IpcResponderController } from "@/ts";
import { getRootPath } from "@/background/utils/getRootPath";

export const openDirDialog: IpcResponderController = (e, args) => {
    const pathType = args.pathType as string;

    const data: { dir: string[]|undefined } = { dir: [] }
    let code = '200';
    let error = undefined;


    try {
        const defaultPath = getRootPath(pathType);
        data.dir = dialog.showOpenDialogSync({ defaultPath, properties: ['openDirectory', 'createDirectory'] });
    } catch (nodeError) {
        error = {
            message: 'Could not ope directory',
            nodeError
        }
        code = '500';
        return { code, error }
    }

    // const storage = args.storage as Storage;
    // const attachmentsPath = getRoot(storage, true);

    if (!data.dir) return { code, error }

    return { data, code, error }
}