import { ref, Ref } from "@vue/composition-api";

const viewType: Ref<'side-by-side'|'markdown'|'formatted'> = ref('side-by-side');

export const useViews = () => {
    const getViewType = () => {
        return viewType;
    };

    const setViewType = (type: 'side-by-side'|'markdown'|'formatted') => {
        viewType.value = type;
    }

    return { getViewType, setViewType }
}
