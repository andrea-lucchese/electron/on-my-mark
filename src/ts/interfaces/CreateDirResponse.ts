export interface CreateDirResponse {
    success: boolean; 
    pathToDir: string;
    relPathToDir: string;
    newDirName: string;
}