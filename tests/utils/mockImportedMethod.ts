/**
 * Mocks a composition method called through its composition function
 * @param {string} modulePath - The name of the composition function/file
 * @param {string} methodName - The name of the method we want to mock
 * @param {(() => unknown)|undefined} mockImplementation - An implementation as jest mock
 */
export const mockImportedMethod = async (
    modulePath: string,
    methodName: string,
    mockImplementation: (() => unknown)|undefined = undefined
) => {
    const importedModule = await import(modulePath);

    mockImplementation = mockImplementation || jest.fn();

    return jest.spyOn(importedModule, methodName).mockImplementation(mockImplementation);
}
