import Vue from 'vue';
import VueCompositionAPI from "@vue/composition-api";

Vue.use(VueCompositionAPI);

import { camelCase } from 'lodash';
import { v4 as generateUuid } from 'uuid';
import { UnwrapRef, reactive, computed } from '@vue/composition-api';
import * as importedActions from '@/actions';
import { ActionParams, AppAction } from '@/ts';

const observerActions = importedActions as unknown as Record<string, AppAction>;

interface Actions extends ActionParams {
    id: string;
    priority: number;
}

/**
 * Collection of observers by subject name.
 * @type {UnwrapRef<{}>}
 */
const observers: UnwrapRef<Record<string, Actions[]>> = reactive({});

/**
 * Actions system, use to subscribe/unsubscribe actions against events.
 * @returns Object
 */
export const useObservers = () => {
    const getSubjects = computed(() => observers);

    /**
     * Removes an entire action from the actions reactive object.
     * @param {string} name
     */
    const unregisterSubject = (name: string) => {
        observers[name] && delete observers[name];
    }

    /**
     * Removes a record by observerId from the observers[eventName] array.
     * @param {string} eventName
     * @param {string} observerId
     */
    const unregisterObserver = (eventName: string, observerId: string) => {
        if (!observers[eventName]) return;

        observers[eventName] = observers[eventName].filter((observer) => {
            return observer.id !== observerId;
        });
    }

    /**
     * Adds an observer against a subject name in the observers reactive object.
     * @param {string} name The action name
     * @param {ObserverName} eventName
     * @param {Record<string, unknown> | undefined} args The arguments to pass to the hook
     * @param {number | undefined} priority The order to execute the hook
     * @return {() => void}
     */
    const registerObserver = (eventName: string, { actionName, args, priority = 10 }: ActionParams) => {
        if (!observers[eventName]) observers[eventName] = [];

        const id = generateUuid();

        /**
         * Removes the registered hook from the actions reactive object.
         */
        const removeSelf = () => {
            unregisterObserver(eventName, id);
        };

        args = { ...args, removeSelf }

        observers[eventName].push({ actionName: actionName, args, priority, id });

        // Sorts again the array by priority
        observers[eventName].sort((a, b) => a.priority - b.priority);

        return removeSelf;
    }

    /**
     * Adds an observer against multiple subjects.
     * @param {eventName[]} eventNames
     * @param {ObserverName} observer
     * @param {Record<string, unknown> | undefined} args
     * @param {number | undefined} priority
     * @return {Record<string, Function>} An object having as a key the subject name and as a value the remove function.
     */
    const registerObserverMultiple = (eventNames: string[], { actionName, args, priority = 10 }: ActionParams) => {
        const unregisterObservers: Record<string, Function> = {};

        eventNames.forEach((eventName) => {
            unregisterObservers[eventName] = registerObserver(eventName, { actionName, args, priority });
        });

        return unregisterObservers;
    }

    /**
     * Performs the observer actions contained in the observers[name] array at the specified index.
     * If the action returns true it will execute the next action in the observers[name] array.
     * @param {string} eventName
     * @param {Record<string, unknown>} args
     * @param {number} index
     * @return {Promise<void>}
     */
    const notifyObservers = async (eventName: string, args: Record<string, unknown> = {}, index = 0) => {
        if (!observers[eventName]) return;

        const subjectObservers = observers[eventName];
        const observer = subjectObservers[index];
        const observerName = camelCase(observer.actionName);
        const observerAction: AppAction = observerActions[observerName];
        const nextAction = await observerAction({...args, ...observer.args});
        const nextIndex = index + 1;

        if (args.oneOff === true) unregisterObserver(eventName, observer.id);

        if (nextAction && subjectObservers[nextIndex] !== undefined) {
            await notifyObservers(eventName, args, nextIndex)
        } else {
            return;
        }
    }

    return { getSubjects, unregisterSubject, unsubscribeObserver: unregisterObserver, registerObserver, registerObserverMultiple, notifyObservers }
}

export default useObservers;
