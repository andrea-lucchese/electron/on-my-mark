import fs from 'fs';
import { trim } from 'lodash';

import { IpcResponderController, AppStorage } from '@/ts';
import { getRoot } from '@/background/utils/getRoot';

/**
 * Scans an array of file/directory names within a given relative path 
 * to determine if they are files or directories and if they have children.
 * @param {IpcMainEvent} e 
 * @param {IpcRendererRequestArgs} args
 * @param {String[]} args.items Files and directory names to scan.
 * @param {AppStorage} args.storage The storage where the file resides.
 * @returns {GetDirContentOfDirItems}
 */
export const readFileContent: IpcResponderController = (e, args) => {
    const fileName = args.fileName as string;
    const storage = args.storage as AppStorage;
    const isAsset = args.isAsset as boolean || false;
    const rootDir = getRoot(storage, isAsset);
    const fullPath = rootDir + '/' + trim(fileName, '/');
    let fileContent = undefined;
    let error;
    let code = '200';

    try {
        const isFile = fs.statSync(fullPath).isFile();
        const encoding = isAsset ? undefined : 'utf8';
        fileContent = isFile ? fs.readFileSync(fullPath, encoding) : '';
    } catch(e) {
        error = {
            message: 'Failed to read content of "' + fullPath + '"',
            nodeError: e
        }
        code = '500';
    }

    
    return { data: fileContent, error, code };
}