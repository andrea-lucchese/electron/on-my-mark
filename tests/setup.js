import Vue from 'vue';

import Vuetify from 'vuetify'
Vue.use(Vuetify);

import VueCompositionAPI from '@vue/composition-api';
Vue.use(VueCompositionAPI);
