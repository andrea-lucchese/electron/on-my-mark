import { useContents } from '@/compositions';
import ipcRendererRequest from '@/ipc/ipcRendererRequest';

/**
 * Requests the current template in plain css.
 * @todo Pass an arg containing the current template (at the moment the background defaults it to 'default').
 */
export const sassRender = async (): Promise<string> => {
    const storage = useContents().getCurrentStorage.value;

    const response = await ipcRendererRequest({
        method: 'GET',
        action: 'sassRender',
        args: { storage }
    });

    const data = response.data as unknown as string;

    return data;
}
