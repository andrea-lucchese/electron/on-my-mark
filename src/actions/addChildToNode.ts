import { buildDirTrailFromDirString } from '@/utils';
import { AppAction, DirContent } from '@/ts';
import { useContents, useTreeView } from '@/compositions';

/**
 * Adds a child to a specified directory.
 * @param args 
 * @returns 
 */
export const addChildToNode: AppAction = async (args) => {
    let relPath = args.relPath as string;
    relPath = relPath || useContents().getCurrentDir.value as string;

    const currentDirTrail: string[] = buildDirTrailFromDirString(relPath);
    const targetNode = useTreeView().getNodeFromPath(currentDirTrail) as DirContent;
    const newNode = useTreeView().buildNode(relPath, args.isDir ? 'dir' : 'file');

    useTreeView().addChildToNode(targetNode, newNode);

    return true;
}
