import { useContents } from "@/compositions/useContents";
import { createFileFromBlob, readFileContent } from "@/ipc";
import { extensionFromContentType } from "@/utils";
import { v4 as uuidv4 } from "uuid";
import { computed } from "@vue/composition-api";
import { NoteAttachment } from "@/ts";

/**
 * Handles files attachments.
 */
export const useAttachments = () => {
    /**
     * Given an array of attachments names requests file contents and returns a collection of
     * urls to the attachment.
     * @param {string[]} attachmentStrings
     * @returns {Promise<Record<string, string>>}
     */
    const getAttachmentBlobUrls = async (attachmentStrings: string[]): Promise<Record<string, string>> => {
        const attachmentBlobs: Record<string, string> = {};

        await Promise.all(
            attachmentStrings.map(async (attachmentString: string) => {
                const fileName = attachmentString.replace('@attachment/', '');
                const currentStorage = useContents().getCurrentStorage.value

                try {
                    const fileContent = await readFileContent(fileName, currentStorage, true) as Uint8Array;

                    const blob = new Blob([ fileContent.buffer ]);
                    const blobUrl = URL.createObjectURL(blob);

                    attachmentBlobs[attachmentString] = blobUrl;
                } catch (e) {
                    // todo: warn the user with a notification
                }

            })
        );

        return attachmentBlobs;
    }

    /**
     * Scans text content for strings prefixed by `@attachment/` and replace them
     * with a blob url to the existing attachment.
     * @param {string} content
     * @returns {string}
     */
    const replaceAttachmentStrings = async (content: string) => {
        // const attachmentStrings = content.match(/@attachment\/[^)]+/g);
        const attachmentImages = [ ...content.matchAll(/!\[([^\]]+)?]\((@attachment\/[^)]+)\)/g) ];
        // const attachmentLinks = content.match(/(?<!!)\[([^\]]+)?]\(@attachment\/[^)]+\)/g);

        if (!attachmentImages) return content;

        const attachmentBlobs = await getAttachmentBlobUrls(attachmentImages.map(attachment => attachment[2]));

        let originalText = content;

        Object.entries(attachmentBlobs).forEach(([ string, replacement ]) => {
            if (originalText.substr(0, 4) === 'blob:') return;
            originalText = originalText.replaceAll(string, replacement);
        });

        return originalText;
    }

    /**
     * Given a clipboard event gets all files contained, if any, saves them to the attachment directory.
     * @param {ClipboardEvent} event
     * @returns {string[]} an array of file names including extensions.
     */
    const retrievePastedFiles = async (event: ClipboardEvent) => {
        const items = event.clipboardData && event.clipboardData.items;

        if (!items) return [];

        const fileNames: string[] = [];

        for (const index in items) {
            const item = items[index];

            if (item.kind === 'file') {
                const extension = extensionFromContentType(item.type);
                const blob: File = item.getAsFile() as File;
                const reader = new FileReader();
                const fileName = uuidv4();

                reader.onload = async () => {
                    if (reader.readyState === 2) {
                        const result = reader.result as string;
                        const buffer = new Buffer(result);
                        await createFileFromBlob(buffer, extension, fileName);
                    }
                };

                const fullAttachmentName = `${ fileName }.${ extension }`;
                reader.readAsArrayBuffer(blob);
                fileNames.push(fullAttachmentName);
                useContents().addMetaAttachment(fullAttachmentName, fullAttachmentName)
            }
        }

        return fileNames;
    }

    const getCurrentAttachments = computed(() => {
        const meta = useContents().getCurrentMeta.value;
        const attachments: NoteAttachment[] = meta.attachments as NoteAttachment[] || [];
        return attachments;
    });

    return {replaceAttachmentStrings, retrievePastedFiles, getCurrentAttachments}
}
