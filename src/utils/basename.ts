export const basename = (path: string): string => {
    return path.replace(/.*\//, '');
}