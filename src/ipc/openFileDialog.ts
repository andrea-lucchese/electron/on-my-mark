import ipcRendererRequest from "@/ipc/ipcRendererRequest";
import { useContents } from "@/compositions";

export const openFileDialog = async () => {
    const storage = useContents().getCurrentStorage.value;
    const timeout = 0;

    const response = await ipcRendererRequest({
        method: 'GET',
        action: 'openFileDialog',
        args: { storage },
        timeout
    });

    return response.data;
}
