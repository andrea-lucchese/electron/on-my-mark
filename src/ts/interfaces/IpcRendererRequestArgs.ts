type IpcRendererRequestUnknownArgs = Record<string, unknown>;

export interface IpcRendererRequestArgs extends IpcRendererRequestUnknownArgs {
    action: string;
    uuid: string;
}