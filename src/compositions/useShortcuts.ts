import Vue from 'vue';
import VueCompositionAPI, { computed, ref, Ref } from "@vue/composition-api";

Vue.use(VueCompositionAPI);

import { camelCase } from 'lodash';
import { buildDirTrailFromDirString } from '@/utils';
import { useObservers } from "@/compositions";
import { getKeyMappings } from "@/ipc";
import { AppContext, KeyMappingConfig, KeyMappings } from "@/ts";

const keyMappings: Ref<KeyMappings|undefined> = ref();

export const useShortcuts = () => {
    const getKey = (event: KeyboardEvent) => {
        const key = event.code.replace('Key', '');
        return key.length > 1 ? key : key.toLowerCase();
    }

    /**
     * Computed reference to the mappings object.
     */
    const getMappings = computed(() => {
        return keyMappings.value;
    });

    /**
     * Given a context, searches the keyMapping object for a valid match.
     * @param {KeyboardEvent} event
     * @param {AppContext} context
     * @return {KeyMappingConfig|undefined}
     */
    const matchConfig = (event: KeyboardEvent, context: AppContext): KeyMappingConfig|undefined => {
        if (!keyMappings.value) return;

        const key = getKey(event);

        const mappingContext = keyMappings.value[context];
        const mappingConfigs = mappingContext && (mappingContext[key] || mappingContext[key]) as KeyMappingConfig[];

        if (!mappingContext || !mappingConfigs) return;

        let match: KeyMappingConfig|undefined = undefined;

        mappingConfigs.some((config) => {

            if (!!config.altKey !== event.altKey) return false;
            if (!!config.shiftKey !== event.shiftKey) return false;
            if (!!config.metaKey !== event.metaKey) return false;
            if (!!config.ctrlKey !== event.ctrlKey) return false;

            match = config;
            return true;
        });

        return match;
    }

    /**
     * Search the keyMappings object for a match recursively in the context hierarchy.
     * @param {KeyboardEvent} event
     * @param {AppContext} context
     * @returns {KeyMappingConfig|undefined}
     */
    const findMapping = (event: KeyboardEvent, context: AppContext): KeyMappingConfig|undefined => {

        const matchedConfig: { value: KeyMappingConfig|undefined } = { value: undefined }

        matchedConfig.value = matchConfig(event, context);

        if (matchedConfig.value) return matchedConfig.value;

        const contextPath = buildDirTrailFromDirString(context, true);

        if (contextPath.length < 2) return;

        contextPath.pop();

        contextPath.some((context) => {
           matchedConfig.value = matchConfig(event, context as AppContext);
           return !!matchedConfig.value;
        });

        return matchedConfig.value;
    }

    /**
     * Determines the current app context.
     * @param event
     * @return {AppContext}
     */
    const getContext = (event: KeyboardEvent) => {
        const target = event.target as HTMLElement;

        if (target.classList.contains('monaco-mouse-cursor-text')) {
            return 'App/NoteContent/MarkdownEditor';
        }

        return target.getAttribute('data-context') as AppContext || 'App';
    }

    /**
     * Initialises the keypress events observers around the keyMappings action names.
     */
    const initObservers = () => {
        if (!keyMappings.value) return;
        Object.values(keyMappings.value).forEach((mappings) => {
            if (!mappings) return;

            Object.values(mappings).forEach((configs) => {

                configs.forEach(config => {
                    const eventName = camelCase(config.action) + 'KeyPress';
                    useObservers().registerObserver(eventName, {
                        actionName: config.action
                    });
                });
            });
        });
    }

    /**
     * Adds a keydown event listener on the window object and handles the mappings.
     */
    const addKeyPressListener = () => {
        window.addEventListener("keydown", async (event) => {
            if (['Control', 'Alt', 'Command', 'Shift'].includes(event.key)) return;

            const context = getContext(event);

            if (!context) return;

            const matchingConfig = findMapping(event, context);

            if (!matchingConfig) return;

            const eventName = camelCase(matchingConfig.action);

            await useObservers().notifyObservers(eventName + 'KeyPress', {
                event: event,
                activeElement: document.activeElement
            });
        }, true);
    }

    /**
     * App key shortcuts initialisation.
     */
    const initShortcuts = async () => {
        keyMappings.value = await getKeyMappings();

        addKeyPressListener();
        initObservers();
    }

    /**
     * Given an action and an app context, generated a string representing the key mapping.
     * @param {string} action
     * @param {AppContext} appContext
     * @return {string}
     */
    const getMappingString = (action: string, appContext: AppContext = 'App') => {
        let mapConfig: KeyMappingConfig|undefined;
        let mapKey = '';
        let mapString = '';

        if (!keyMappings.value || !action) return '';

        Object.entries(keyMappings.value).some(([context, mappings]) =>{
            if (context !== appContext) return false;
            if (!mappings) return true;

            Object.entries(mappings).some(([key, mapping]) => {
                mapping.forEach(map => {
                    if(map.action === action) {
                        mapConfig = map;
                        mapKey = key;
                        return true;
                    }
                });

                return mapConfig !== undefined;
            });
        });

        if (!mapConfig) return '';

        mapString = mapKey.toUpperCase();
        mapString += mapConfig.altKey ? ' + Alt' : '';
        mapString += mapConfig.shiftKey ? ' + Shift' : '';
        mapString += mapConfig.ctrlKey ? ' + Ctrl' : '';
        mapString += mapConfig.metaKey ? ' + Meta' : '';

        return mapString;
    }

    return { initShortcuts, getMappingString, getMappings }
}
