import { AppAction } from "@/ts";
import { useTreeView } from "@/compositions";
import { getNode } from "@/utils";

export const makeLabelEditable: AppAction = (args) => {
    const activeElement = args.activeElement as HTMLElement;
    const path = activeElement.getAttribute('data-path') as string;
    const treeNode = getNode(path);

    useTreeView().openItem(treeNode);

    return true;
}
