import fs from 'fs';
import { app } from 'electron';
import { IpcResponderController } from '@/ts';
import { getPathToAppSettings } from "@/background/utils";

export const getSettings: IpcResponderController = (e, args) => {
    const paths = {
        appData: app.getPath('appData'),
        userData: app.getPath('userData'),
        cache: app.getPath('cache'),
        temp: app.getPath('temp'),
        module: app.getPath('module'),
        desktop: app.getPath('desktop'),
        documents: app.getPath('documents'),
        downloads: app.getPath('downloads')
    }
    const pathTosettingFile = getPathToAppSettings();
    let code = '200';
    let error;
    let data;

    try {
        const settingsPlainText = fs.readFileSync(pathTosettingFile, 'utf8');
        const settings = JSON.parse(settingsPlainText);
        settings.paths = paths;
        const settingName: string|undefined = args.name as string;
        data = settingName ? settings[settingName] : settings; 
    } catch (nodeError) {
        code = '500';
        error = { 
            message: `Could not retrieve settings, path to file: `+ pathTosettingFile + ' path:' , 
            nodeError }
    }


    return { data, code, error }
}