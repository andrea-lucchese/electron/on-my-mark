import '@/../tests/setup';
import { useContents } from "@/compositions";
import { RenameAttachmentResult } from "@/ts";

describe("useContents.ts - removeMetaAttachment", () => {
    it('removeMetaAttachment removes an attachment from the meta object', () => {
        const meta = {
            attachments: [
                { name: 'attachment-1', label: 'Attachment 1' },
                { name: 'attachment-2', label: 'Attachment 2' },
                { name: 'attachment-3', label: 'Attachment 3' },
                { name: 'attachment-4', label: 'Attachment 4' }
            ]
        }

        useContents().setCurrentMeta(meta);

        const removeMetaAttachment = useContents().removeMetaAttachment;

        removeMetaAttachment('attachment-2');

        expect(meta.attachments.length).toEqual(3);

        expect(meta.attachments).toEqual([
            { name: 'attachment-1', label: 'Attachment 1' },
            { name: 'attachment-3', label: 'Attachment 3' },
            { name: 'attachment-4', label: 'Attachment 4' }
        ]);

        removeMetaAttachment('attachment-1');

        expect(meta.attachments).toEqual([
            { name: 'attachment-3', label: 'Attachment 3' },
            { name: 'attachment-4', label: 'Attachment 4' }
        ]);

        removeMetaAttachment('attachment-4');

        expect(meta.attachments).toEqual([
            { name: 'attachment-3', label: 'Attachment 3' }
        ]);
    });

    it('When attachment is not in the meta, calling removeMetaAttachment do nothing', () => {
        const meta = { attachments: [
            { name: 'attachment-3', label: 'Attachment 3' }
        ]}

        useContents().setCurrentMeta(meta);

        useContents().removeMetaAttachment('another-name');

        expect(meta.attachments).toEqual([ { name: 'attachment-3', label: 'Attachment 3' } ]);

    });
});

describe("useContents.ts - renameMetaAttachment()", () => {

    it('renameMetaAttachment(): sets a new label correctly abd returns 1', () => {
        const meta = {
            attachments: [
                { name: 'attachment-1', label: 'Attachment 1' },
                { name: 'attachment-2', label: 'Attachment 2' },
                { name: 'attachment-3', label: 'Attachment 3' },
                { name: 'attachment-4', label: 'Attachment 4' }
            ]
        }

        useContents().setCurrentMeta(meta);

        const newLabel = 'My new cool label';

        const renameMetaAttachment = useContents().renameMetaAttachment;

        const result = renameMetaAttachment('attachment-2', newLabel);

        expect(meta.attachments[1].label).toEqual(newLabel);
        expect(result).toEqual(RenameAttachmentResult.LABEL_RENAMED_SUCCESSFULLY);
    });

    it('renameMetaAttachment() attachment not found, returns 2', () => {
        const meta = {
            attachments: [
                { name: 'attachment-1', label: 'Attachment 1' },
                { name: 'attachment-2', label: 'Attachment 2' },
                { name: 'attachment-3', label: 'Attachment 3' },
                { name: 'attachment-4', label: 'Attachment 4' }
            ]
        }

        useContents().setCurrentMeta(meta);

        const newLabel = 'My new cool label';

        const renameMetaAttachment = useContents().renameMetaAttachment;

        const result = renameMetaAttachment('attachment-not-exists', newLabel);

        expect(meta.attachments).toEqual(meta.attachments);
        expect(result).toEqual(RenameAttachmentResult.ATTACHMENT_DOES_NOT_EXISTS);
    });

    it('renameMetaAttachment() label already exists, returns 0', () => {
        const meta = {
            attachments: [
                { name: 'attachment-1', label: 'Attachment 1' },
                { name: 'attachment-2', label: 'Attachment 2' },
                { name: 'attachment-3', label: 'Attachment 3' },
                { name: 'attachment-4', label: 'Attachment 4' }
            ]
        }

        useContents().setCurrentMeta(meta);

        const existingLabel = meta.attachments[3].label;

        const renameMetaAttachment = useContents().renameMetaAttachment;

        const result = renameMetaAttachment('attachment-2', existingLabel);

        expect(meta.attachments).toEqual(meta.attachments);
        expect(result).toEqual(RenameAttachmentResult.LABEL_ALREADY_EXISTS);
    });
});