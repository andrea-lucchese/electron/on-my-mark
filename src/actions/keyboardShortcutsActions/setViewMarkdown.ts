import { useViews } from "@/compositions";

/**
 * Sets the current view to 'markdown'.
 */
export const setViewMarkdown = () => {
    useViews().setViewType('markdown');
}
