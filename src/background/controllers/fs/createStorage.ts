import { concatPaths, readSettings, rootPrefix, writeSettings } from '@/background/utils';
import { IpcResponderController, AppStorage } from '@/ts';
import fs from 'fs';
import { getRootPath } from "@/background/utils/getRootPath";

export const createStorage: IpcResponderController = (e, args) => {
    const storage = args.storage as AppStorage;
    let code = '200';
    let error;
    let data: Record<string, unknown> = {};
    let rootDir = '';

    try {
        const settings = readSettings();

        if (storage.type === 'absolute' && storage.path) {
            rootDir = storage.path;
        } else {
            rootDir = getRootPath(storage.root);
            rootDir = concatPaths([rootDir, storage.path || ''], rootPrefix());
        }

        if (!fs.existsSync(rootDir)) fs.mkdirSync(rootDir);

        const storageDir = concatPaths([rootDir, 'storage'], rootPrefix() );
        const assetsDir = concatPaths([rootDir, 'assets'], rootPrefix());

        if (!fs.existsSync(storageDir)) fs.mkdirSync(storageDir);
        if (!fs.existsSync(assetsDir)) fs.mkdirSync(assetsDir);

        settings.storage.push(storage);

        writeSettings(settings);

        data = {
            storage: settings.storage,
            newStorage: storage
        };

    } catch (nodeError) {
        code = '500';
        error = { message: `Could not create storage`, nodeError }
    }

    return { data, code, error }
}
