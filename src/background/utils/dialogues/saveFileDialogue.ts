import { dialog, FileFilter } from "electron";

type DialogueProperties = 'showOverwriteConfirmation'|'createDirectory';

interface  SaveFileDialogArgs {
    title?: string;
    filters?: FileFilter[];
    properties?: DialogueProperties[];
}

/**
 * Prompts a save file dialogue.
 * @param args
 */
export const saveFileDialog = (args: SaveFileDialogArgs = {}) => {
    const title = args.title;
    const filters = args.filters || [{ name: 'All Files', extensions: ['*'] }];
    const properties: DialogueProperties[] = ['createDirectory', 'showOverwriteConfirmation'];

    const pathToFile = dialog.showSaveDialogSync({ properties, title, filters });

    if (!pathToFile) return;

    return pathToFile;
}
