import { IpcResponderController } from '@/ts';
import { defineControllerDefaults, readSettings, writeSettings } from "@/background/utils";

export const setAppTheme: IpcResponderController = (e, args) => {
    const response = defineControllerDefaults();
    const appTheme = args.appTheme as 'dark'|'light';

    try {
        const settings = readSettings();
        settings.appTheme = appTheme;
        writeSettings(settings);
        response.data = settings;
    } catch (nodeError) {
        response.code = '500';
        response.error = { message: 'Could nor write settings file.', nodeError }
    }

    return response;
}
