import { useContents } from "@/compositions";

export const switchDarkMode = () => {
    const isDarkMode = useContents().isDarkMode.value;
    if (isDarkMode) {
        useContents().setCurrentTheme('light');
    } else {
        useContents().setCurrentTheme('dark');
    }
}
