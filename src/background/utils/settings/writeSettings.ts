import fs from 'fs';
import { AppSettings } from "@/ts/interfaces/AppSettings";
import { getPathToAppSettings } from "@/background/utils";

/**
 * Writes a settings object to the settings file.
 * @param {AppSettings} settings
 */
export const writeSettings = (settings: AppSettings) => {
    const pathToAppSettings = getPathToAppSettings();

    const settingsString = JSON.stringify(settings);
    fs.writeFileSync(pathToAppSettings, settingsString);
}