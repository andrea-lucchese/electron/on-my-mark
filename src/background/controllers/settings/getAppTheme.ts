import { IpcResponderController } from '@/ts';
import { defineControllerDefaults, readSettings } from "@/background/utils";

export const getAppTheme: IpcResponderController = () => {
    const response = defineControllerDefaults();

    try {
        const settings = readSettings();
        response.data = settings.appTheme;
    } catch (nodeError) {
        response.code = '500';
        response.error = { message: 'Could nor read settings file.', nodeError }
    }

    return response;
}
