import { AppAction } from "@/ts";
import { useTreeView } from "@/compositions";
import { getNode } from "@/utils";

/**
 * Navigates one node down the tree view, if a node has children and is open navigates inside.
 * @param args
 */
export const navigateTreeDown: AppAction = (args) => {
    const {
        getNextNode,
        getFirstChild,
        getElementFromNode,
        focusOnNextNode,
        isNodeOpen,
        getParentNode
    } = useTreeView();

    const activeElement = args.activeElement as HTMLElement;
    const path = activeElement.getAttribute('data-path') as string;
    const treeNode = getNode(path);
    const nextNode = getNextNode(treeNode);

    if (treeNode.children && treeNode.children.length && isNodeOpen(treeNode)) {
        const firstChild = getFirstChild(treeNode);
        getElementFromNode(firstChild).focus();
    } else if (nextNode) {
        focusOnNextNode(treeNode);
    } else {
        const parentNode = getParentNode(treeNode);
        focusOnNextNode(parentNode);
    }

    return true;
}
