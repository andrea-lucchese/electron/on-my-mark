import * as fs from "fs";
import { getAppDir, readSettings } from "@/background/utils";
import { concatPaths } from "@/utils";
import { getRootPath } from "@/background/utils/getRootPath";
import os from 'os';
import { Menu } from "electron";

const appInit = () => {
   const pathToHome = getAppDir();
   const pathToSettingsDir = `${pathToHome}/settings`;
   const prefix = os.platform() === 'win32' ? '' : '/';

   /**
    * Creates settings dir if it does not exist.
    */
   if (!fs.existsSync(pathToSettingsDir)) {
      fs.mkdirSync(pathToSettingsDir, { recursive: true });
   }

   if (process.env.NODE_ENV !== 'development') {
       new Menu();
       Menu.setApplicationMenu(null);
   }

   const pathToSettingsFile = `${pathToSettingsDir}/settings.json`;

   /**
    * Creates default settings file if it does not exist.
    */
   if (!fs.existsSync(pathToSettingsFile)) {
      // eslint-disable-next-line @typescript-eslint/ban-ts-ignore
      // @ts-ignore
      const pathToDefaultsSettings = concatPaths([__static, 'defaults', 'settings.json' ], prefix);

      fs.copyFileSync(pathToDefaultsSettings, pathToSettingsFile);
   }

   const settings = readSettings();

   /**
    * Reads the settings and storage folders if they do not exist yet.
    * @todo In case of absolute storages instead change the type to 'absolute:disabled'
    */
   settings.storage.forEach((vault) => {
      let storagePath = '';
      if (vault.type === 'absolute') {
         storagePath = vault.path || '';
      } else {
         const storageRoot = getRootPath(vault.root);
         storagePath = concatPaths([storageRoot, vault.path || ''], prefix);
      }

      const storageDir = concatPaths([storagePath, 'storage'], prefix);
      const assetsDir = concatPaths([storagePath, 'assets'], prefix);

      if (!fs.existsSync(storageDir)) {
         fs.mkdirSync(storageDir, { recursive: true });
      }

      if (!fs.existsSync(assetsDir)) {
         fs.mkdirSync(assetsDir, { recursive: true });
      }
   });

   /**
    * Creates default keymapping stylesheet if it does not exist.
    */
   const pathToKeyMappingsFile = `${pathToSettingsDir}/key-mappings.json`;

   if (!fs.existsSync(pathToKeyMappingsFile)) {
      // eslint-disable-next-line @typescript-eslint/ban-ts-ignore
      // @ts-ignore
      const pathToDefaultKeyMappings = concatPaths([__static, 'defaults', 'key-mappings.json' ], prefix);

      fs.copyFileSync(pathToDefaultKeyMappings, pathToKeyMappingsFile);
   }

   /**
    * Creates default stylesheet file if it does not exist.
    */
   const pathToStyles = `${pathToSettingsDir}/.default.scss`;

   if (!fs.existsSync(pathToStyles)) {
      // eslint-disable-next-line @typescript-eslint/ban-ts-ignore
      // @ts-ignore
      const pathToDefaultStyles = concatPaths([__static, 'defaults', 'default-md-theme.scss' ], prefix);

      fs.copyFileSync(pathToDefaultStyles, pathToStyles);
   }
}

export default appInit;
