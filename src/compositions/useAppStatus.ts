import Vue from 'vue';
import VueCompositionAPI from "@vue/composition-api";

Vue.use(VueCompositionAPI);

import { computed, ref, Ref } from "@vue/composition-api";

const appReady: Ref<boolean> = ref(false);

export const useAppStatus = () => {
    const isReady = computed((): boolean => {
        return appReady.value;
    });

    const setAppReady = (isReady: boolean) => {
        appReady.value = isReady;
    }

    return { isReady, setAppReady }
}