import os from 'os';

export const rootPrefix = () => {
    return os.platform() === 'win32' ? '' : '/';    
}