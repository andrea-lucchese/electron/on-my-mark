import ipcRendererRequest from '@/ipc/ipcRendererRequest';
import { KeyMappings } from "@/ts";

/**
 * Requests to change the appTheme settings parameter.
 */
export const getKeyMappings = async (): Promise<KeyMappings> => {
    const response = await ipcRendererRequest({
        method: 'GET',
        action: 'getKeyMappings',
        args: {}
    });

    return response.data as KeyMappings;
}
