import ipcRendererRequest from "@/ipc/ipcRendererRequest";

/**
 * Performs a request to export a file as PDF.
 * @param {string} rawHtml - Plain html string.
 * @returns {string} - The export location.
 */
export const exportToPdf = async (rawHtml: string) => {
    const response = await ipcRendererRequest({
        method: 'GET',
        action: 'exportToPdf',
        args: {  rawHtml },
        timeout: 0
    });

    const data = response.data as {
        exportLocation: string;
    };

    return data.exportLocation;
}
