import ipcRendererRequest from "@/ipc/ipcRendererRequest";
import { OpenDirDialogResponse } from "@/ts";

export const openDirDialog = async (pathType: string): Promise<OpenDirDialogResponse> => {
    const timeout = 0;

    const response = await ipcRendererRequest({
        method: 'GET',
        action: 'openDirDialog',
        args: { pathType },
        timeout
    });

    const data = response.data as OpenDirDialogResponse || {};

    return data;
}
