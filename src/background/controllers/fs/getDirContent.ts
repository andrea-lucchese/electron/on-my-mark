import fs from 'fs';
import { getDirContentOfDirItems } from './getDirContentOfDirItems';
import { IpcResponderController, IpcResponderControllerResponse, DirContent, AppStorage } from '@/ts';
import { getRoot } from '@/background/utils/getRoot';
import { trim } from 'lodash';
import * as path from "path";
import { concatPaths } from "@/utils";
import { rootPrefix } from '@/background/utils';

interface GetDirContentResponse extends IpcResponderControllerResponse {
    data: DirContent[];
}

/**
 * Given a relative path to directory extract contents 1 level deep.
 * @param {IpcMainEvent} e 
 * @param {IpcRendererRequestArgs} args
 * @param {String|undefined} args.dirName The directory we want to read.
 * @returns {GetDirContentResponse}
 */
const getDirContent: IpcResponderController = (e, args) => {
    const defaults = {
        dirsFirst: true,
        sortBy: 'name',
        sort: 'ASC'
    }

    args = { ...defaults, ...args }

    const relPath = args.relPath as string || '';
    const storage = args.storage as AppStorage;
    const rootDir = getRoot(storage);
    const directory = rootDir + '/' + trim(relPath, '/');
    let data;
    let code = '200';
    let error;

    if (!rootDir) return { code: '500', error: { message: 'No root provided' } }

    try {
        const items: string[] = fs.readdirSync(directory).filter((item) => {
            const itemPath = concatPaths([rootDir, relPath, item], rootPrefix());
            const isDir = fs.statSync(itemPath).isDirectory();
            const extension = path.extname(item);

            if (isDir) return true;
            if (extension === '.md') return true;
        });

        const dirContent = getDirContentOfDirItems(e, { ...args, items, storage }) as IpcResponderControllerResponse;
    
        const contents = dirContent.data as DirContent[];
        const dirs: DirContent[] = [];
        const files: DirContent[] = [];
    
        contents.forEach(item => {
            if (item.type === 'dir') dirs.push(item);
            else files.push(item);
        });
    
        data = args.dirsFirst ? dirs.concat(files) : files.concat(dirs);
    } catch(nodeError) {
        code = '500';
        error = { message: `Could not read content of "${directory}"`, nodeError }
    }
    
    return { data, error, code };
}

export { getDirContent }
