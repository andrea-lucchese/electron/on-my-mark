<template>
    <v-treeview
        ref="treeRef"
        :items="treeContents"
        :open.sync="treeOpenItems"
        :active.sync="treeActiveItems"
        :activatable="false"
        return-object
        item-key="treeViewKey"
        hoverable
        selection-type="independent"
        @update:open="handleOpen"
        @update:active="handleActivate"
    >
        <template v-slot:label="{ item }">
            <node-label
                tabindex="0"
                :aria-label="item.name"
                :item="item"
                :data-path="item.path"
                @click="handleLabelClick"
                @focus="handleLabelFocus"
            />
        </template>

        <template v-slot:prepend="{ item, open }">
            <v-icon v-if="item.type ==='dir'">
                {{ open ? 'mdi-folder-open' : 'mdi-folder' }}
            </v-icon>
            <v-icon v-else small>
                {{ 'mdi-file-document-outline' }}
            </v-icon>
        </template>
    </v-treeview>
</template>

<script lang="ts">
import {
    ComputedRef,
    defineComponent,
    nextTick,
    onBeforeMount,
    Ref,
    ref,
    watch,
} from "@vue/composition-api";
import { VueConstructor } from "vue";
import { ContextMenuAction, ContextMenuArgs, DirContent } from "@/ts";
import { useContents, useAppStatus, useContextMenu, useObservers, useTreeView } from "@/compositions";
import { dirname, createNewFile, makeElementSelectable } from "@/utils";
import { createDir, deleteFile } from "@/ipc";
import { difference } from 'lodash';
import NodeButton from "@/components/ViewTree/NodeButton.vue";
import ContentEditable from "@/components/ContentEditable.vue";
import NodeLabel from "@/components/ViewTree/NodeLabel.vue";

interface LabelClick {
    count: number;
    timeout: NodeJS.Timeout|undefined;
}

export default defineComponent({
    components: {NodeButton, ContentEditable, NodeLabel},
    name: "AppTreeView",
    setup() {
        /**
         * The treeview data root directory.
         */
        const rootDir: ComputedRef<DirContent> = useTreeView().getRoot;
        const treeOpenItems = useTreeView().getOpenItems();
        const treeActiveItems = useTreeView().getActiveItems();
        const treeContents: Ref<DirContent[] | undefined> = useTreeView().getTreeContents;
        const appReady: ComputedRef<boolean> = useAppStatus().isReady;
        const openMenu = useContextMenu().openMenu;
        const previouslyOpen: { items: string[] } = {items: []};
        const treeRef: Ref<VueConstructor | undefined> = ref();
        const clicks: LabelClick = { count: 0, timeout: undefined }

        const {
            updateTreeContents,
            loadNodesChildren
        } = useTreeView();

        /**
         * Handles tree node activation by setting the current dir or file.
         * @param {String[]} activeItems - Array containing the trailing path to the node.
         */
        const handleActivate = (activeItems: DirContent[]) => {
            let node;

            if (!activeItems || !activeItems.length) {
                node = useTreeView().getRoot.value;
            } else {
                const lastIndex = activeItems.length - 1;
                node = activeItems[lastIndex];
            }


            if (node.type === 'dir') {
                useContents().setCurrentDir(node.path);
            } else {
                const dirName = dirname(node.path);
                useContents().setCurrentDir(dirName);
            }

            useContents().setCurrentFile(node.path);
        };

        /**
         * Handles node open loading children of target node children.
         * Eg. if target is /Node/Child it will load children of /Node/Child/Child
         * @param {String[]|string} allSelectedItems
         */
        const handleOpen = async (allSelectedItems: DirContent[]) => {
            const selectedItemsKeys: string[] = allSelectedItems.map((item: DirContent) => item.treeViewKey);

            const openedItem = difference(selectedItemsKeys, previouslyOpen.items);

            previouslyOpen.items = selectedItemsKeys;

            if (!openedItem.length || !allSelectedItems.length) return;

            const filteredItems = allSelectedItems.filter((item: DirContent) => {
                return item.treeViewKey === openedItem[0];
            });

            const targetNode = filteredItems[0];

            const children = targetNode && targetNode.children;

            const childrenNames = children && children.map((child: DirContent) => child.name);

            if (!childrenNames || !targetNode) return;

            await loadNodesChildren(childrenNames, targetNode);
        };

        const handleItemDeleteClick = async (event: MouseEvent, item: DirContent) => {
            event.preventDefault();
            const relPath = item.path;

            await deleteFile(relPath);

            await useObservers().notifyObservers("itemDeleted", {
                deletedItem: item,
            });
        };

        const handleAddFolderClick = async (
            event: MouseEvent,
            item: DirContent
        ) => {
            const response = await createDir(item.path);

            await useObservers().notifyObservers("newDirCreated", {
                dirName: response.newDirName,
                relPath: response.relPathToDir,
                isDir: true,
            });

            await nextTick();

            useTreeView().openItem(item.treeViewKey);
        };

        const toggleNodeCollapsed = (item: DirContent, isRenaming: boolean) => {
            if (isRenaming) return;

            if (item.type === 'file') {
                treeActiveItems.value = [ item ];
            } else {
                const isOpen = useTreeView().isNodeOpen(item);

                if (isOpen) useTreeView().closeItem(item);
                else useTreeView().openItem(item);
            }
        }

        /**
         * Handles single/double click event on node label.
         * - Single click event: toggle the node open/close status.
         * - Double click do noting (handled by the ContentEditable component).
         * @param {DirContent} item
         * @param {boolean} isRenaming
         */
        const handleLabelClick = (item: DirContent, isRenaming: boolean) => {
            clicks.count++

            clicks.timeout && clearTimeout(clicks.timeout);

            if (clicks.count > 1) {
                clicks.count = 0;
                return;
            }

            clicks.timeout = setTimeout(() => {
                clicks.count = 0;
                toggleNodeCollapsed(item, isRenaming);
            }, 300);
        }

        /**
         * When a label receives an mouse focus event, if it's a file, activate the file.
         */
        const handleLabelFocus = (item: DirContent) => {
            if (item.type === 'dir') return;
            treeActiveItems.value = [ item ];
        };

        const openContextMenu = (event: MouseEvent, item: DirContent) => {
            const context = 'TreeView/item/' + item.type;
            const args = item as unknown as ContextMenuArgs;

            openMenu(event, args, context);
        }

        const deleteItem: ContextMenuAction = (e, args) => {
            const item = args as unknown as DirContent;

            handleItemDeleteClick(e, item);
        }

        const addFolder: ContextMenuAction = (e, args) => {
            const item = args as unknown as DirContent;

            handleAddFolderClick(e, item);
        }

        const addFile: ContextMenuAction = async (e, args) => {
            const parentDir = args as unknown as DirContent;

            await nextTick();

            await createNewFile(parentDir.path);

            await nextTick();
        }

        /**
         * Triggers editing of the current node.
         * @param {MouseEvent} e
         * @param {Record<string, unknown>} args
         */
        const editNodeName: ContextMenuAction = (e) => {
            const target = e.target as HTMLElement;

            makeElementSelectable(target);
        }

        /**
         * Uses editNodeName to make a folder label content editable.
         * @param {MouseEvent} e
         * @param {Record<string, unknown>} args
         */
        const renameFolder: ContextMenuAction = (e, args) => {
            editNodeName(e, args);
        }

        /**
         * Uses editNodeName to make a file label content editable.
         * @param {MouseEvent} e
         * @param {Record<string, unknown>} args
         */
        const renameNote: ContextMenuAction = (e, args) => {
            editNodeName(e, args);
        }

        /**
         * Registers the treeview context menu items.
         */
        const registerContextMenuItems = () => {
            useContextMenu().registerMenuItems([
                {
                    name: 'delete-note',
                    title: 'Delete Note',
                    context: 'TreeView/item/file',
                    action: deleteItem
                },
                {
                    name: 'rename-note',
                    title: 'Rename Note',
                    context: 'TreeView/item/file',
                    action: renameNote
                },
                {
                    name: 'rename-folder',
                    title: 'Rename Folder',
                    context: 'TreeView/item/dir',
                    action: renameFolder
                },
                {
                    name: 'delete-folder',
                    title: 'Delete Folder',
                    context: 'TreeView/item/dir',
                    action: deleteItem
                },
                {
                    name: 'add-folder',
                    title: 'Add Folder',
                    context: 'TreeView/item/dir',
                    action: addFolder
                },
                {
                    name: 'add-file',
                    title: 'Add Note',
                    context: 'TreeView/item/dir',
                    action: addFile
                }
            ]);
        }

        /**
         * Registers component-related observers.
         */
        const registerObservers = () => {
            useObservers().registerObserver("itemDeleted", {
                actionName: "REMOVE_TREE_NODE",
            });

            useObservers().registerObserver("storageHasChanged", {
                actionName: "CLEAR_PREVIOUSLY_OPEN_ITEMS",
                args: {
                    clearItems: () => {
                        previouslyOpen.items = []
                    }
                }
            });
        }

        onBeforeMount(() => {
            if (appReady.value) updateTreeContents();

            registerObservers();

            registerContextMenuItems();
        });

        watch(appReady, (isAppReady: boolean) => {
            if (!isAppReady) return;

            updateTreeContents();
        });

        return {
            treeContents,
            rootDir,
            handleActivate,
            handleOpen,
            handleItemDeleteClick,
            handleAddFolderClick,
            treeOpenItems,
            handleLabelClick,
            handleLabelFocus,
            treeActiveItems,
            openContextMenu,
            treeRef
        };
    },
    props: {value: Boolean},
});
</script>

<style lang="scss" scoped>
.mn-main-sidebar {
    top: 0 !important;
    max-height: 100vh !important;
}

::v-deep .v-navigation-drawer__border {
    width: 10px !important;
    cursor: col-resize;
    transition: initial;
    background-color: var(--v-secondary-base) !important;
}

.v-treeview-node__content {
    .mn-treeview-label {
        cursor: pointer;
    }

    .mn-treeview-item-actions {
        display: none;
    }

    &:hover {
        .mn-treeview-item-actions {
            display: block;
        }
    }
}
</style>