import { KeyMappingConfig } from "@/ts";

export type KeyMapping = Record<KeyboardEvent['key'], KeyMappingConfig[]>
