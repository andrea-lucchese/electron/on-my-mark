import { useTreeView } from "@/compositions";
import { focusOnFirstTreeNode } from "@/actions";

/**
 * Sets the focus to the last focused element of the tree or the first child of the tree
 * if no previous focused element was found.
 */
export const focusOnTree = () => {
    const lastFocusedNode = useTreeView().getLastFocusedItem.value;

    if (lastFocusedNode) {
        const targetTreeNode = useTreeView().getElementFromNode(lastFocusedNode);
        targetTreeNode && targetTreeNode.focus();
    } else {
        focusOnFirstTreeNode();
    }
}
