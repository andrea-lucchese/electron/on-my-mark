import Vue from 'vue';
import VueCompositionAPI, { nextTick } from "@vue/composition-api";

Vue.use(VueCompositionAPI);
import { dirname, basename, extname } from 'path';
import { v4 as uuidv4 } from 'uuid';
import { ComputedRef, computed, Ref, ref, set } from "@vue/composition-api";
import { getDirContentOfItems, getTreeContents as loadTreeContents } from "@/ipc";
import { DirContent } from '@/ts';
import { useContents } from "@/compositions/useContents";
import {
    buildDirTrailFromDirString,
    getNodeChildren,
    getNodeSiblings,
    makeElementSelectable,
    sortDirContents
} from "@/utils";

interface TreeNodes {
    children?: DirContent[];
    targetNode: DirContent | undefined;
}

const baseTreeContents: DirContent = {
    name: '',
    path: '/',
    isRoot: true,
    type: 'dir',
    children: [],
    treeViewKey: '/'
}

/**
 * The variable used to populate the treeview component.
 * @type {Ref<DirContent>}
 */
const treeContents: Ref<DirContent> = ref({ ...baseTreeContents });

/**
 * A collection of the keys of the currently opened items.
 * @type {Ref<string[]>}
 */
const treeOpenItems: Ref<DirContent[]> = ref([]);

/**
 * A collection of currently active items.
 * @type {Ref<DirContent[]>}
 */
const treeActiveItems: Ref<DirContent[]> = ref([]);

const lastFocusedItem: Ref<DirContent|undefined> = ref()
/**
 * Contains methods and variables to manage the treeview and its contents.
 */
export const useTreeView: Function = () => {
    const getRoot: ComputedRef<DirContent> = computed(() => {
        return treeContents.value;
    });

    /**
     * Gets the currently selected node.
     * @returns {DirContent}
     */
    const getCurrentNode = computed(() => {
        return useContents().getCurrentNodeObject.value;
    });

    /**
     * Gets the children of the root directory.
     * @type {DirContent[]}
     */
    const getTreeContents = computed(() => {
        return treeContents.value.children as DirContent[];
    });


    /**
     * Gets the current node siblings, including the current node.
     * @returns {DirContent[]}
     */
    const getCurrentNodeSiblings = computed(() => {
        const currentNode = getCurrentNode.value;
        return getNodeSiblings(currentNode.path);
    });

    const getLastFocusedItem = computed(() => {
        return lastFocusedItem.value;
    });

    const isNodeOpen = (node: DirContent) => {
        if (!treeOpenItems.value || !treeOpenItems.value.length) return false;

        return treeOpenItems.value.some((item) => {
            if (item.treeViewKey !== node.treeViewKey) return false;

            return true;
        });
    }

    /**
     * Sets the lastFocusedItem property to keep track of which was the last item focused on the node.
     * @param {DirContent} item
     */
    const setLastFocusedItem = (item: DirContent) => {
        lastFocusedItem.value = item;
    }

    /**
     * Sets the value of the whole tree (the root element and its children).
     * @param {DirContent} rootDir
     */
    const setRoot = (rootDir: DirContent) => {
        treeContents.value = rootDir;
    }

    /**
     * Sets the treeContents value.
     * @param {DirContent[]} contents 
     */
    const setTreeContents = (contents: DirContent[]) => {
        const newTreeContents = { ...baseTreeContents };
        newTreeContents.children = contents;
        treeContents.value = newTreeContents;
    }

    /**
     * Sets the children of the root element.
     * @param {DirContent[]} children
     */
    const setRootChildren = (children: DirContent[]) => {
        treeContents.value.children = children;
    }

    /**
     * Requests the root directory contents and sets the treeContents value.
     */
    const updateTreeContents = async () => {
        const treeContentsValue = await loadTreeContents() as DirContent[];
        const newRootDir = { ...baseTreeContents }
        newRootDir.children = treeContentsValue;

        treeContents.value = newRootDir;
    }

    /**
     * Returns the treeOpenItems as a modifiable reference.
     * @type {string[]}
     */
    const getOpenItems = () => {
        return treeOpenItems;
    };

    /**
     * Returns the treeActiveItems as a modifiable reference.
     * @type {string[]}
     */
    const getActiveItems = () => {
        return treeActiveItems;
    };

    /**
     * Sets the active items.
     * @param items
     */
    const setActiveItems = (items: DirContent[]) => {
        treeActiveItems.value = items;
    }

    /**
     * Adds a node to the treeOpenItems array causing the corresponding item to show open in the treeview.
     * @param {DirContent} dirNode
     */
    const openItem = (dirNode: DirContent) => {
        if (!dirNode || !dirNode.children || !dirNode.children.length) return;
        if (treeOpenItems.value.includes(dirNode)) return;

        treeOpenItems.value.push(dirNode);
    }

    /**
     * Removes an node to the treeOpenItems array causing the corresponding item to show closed in the treeview.
     * @param {DirContent} dirNode
     */
    const closeItem = (dirNode: DirContent) => {
        if (!dirNode.children || !dirNode.children.length) return;
        if (!treeOpenItems.value.includes(dirNode)) return;

        treeOpenItems.value = treeOpenItems.value.filter((item) => {
            return item.treeViewKey !== dirNode.treeViewKey;
        });
    }

    /**
     * Search for a node in the treeview items.
     * @todo IMPORTANT: If it doesn't find the node returns the previous node, causing infinite loop: fix it!
     * @param {string[]} pathToNode
     * @returns {DirContent|undefined}
     */
    const getNodeFromPath = (pathToNode: string[]): DirContent | undefined => {
        if (pathToNode.length === 1 && pathToNode[0] === '/') return getRoot.value;

        const treeNodes: TreeNodes = { children: treeContents.value.children, targetNode: undefined };

        pathToNode = pathToNode.sort();

        // At each iteration searches one level deeper
        pathToNode.forEach((path) => {

            treeNodes.children && treeNodes.children.some(targetNode => {

                if (targetNode.path === path) {

                    treeNodes.children = targetNode.children;
                    treeNodes.targetNode = targetNode || {};
                    return true;
                }
            });
        });

        return treeNodes.targetNode || undefined;
    }

    /**
     * Given a treeview key, returns the relative DOM element.
     * @param {string} nodeKey
     * @return {HTMLElement|null}
     */
    const getElementFromNodeKey = (nodeKey: string): HTMLElement|null => {
        return document.getElementById(nodeKey);
    }

    /**
     * Given a treeview node, returns the relative DOM element.
     * @param {DirContent} node
     * @return {HTMLElement|null}
     */
    const getElementFromNode = (node: DirContent) => {
        return getElementFromNodeKey(node.treeViewKey);
    }

    /**
     * Given an array of children (represented by path) search for children and adds them to the treeContent.
     */
    const loadNodesChildren = async (childrenPaths: string[], parentNode: DirContent) => {
        if (!parentNode.children) return;

        /**
         * The children of the targetNode as returned by the background.
         * @type {DirContent[]}
         */
        const children = await getDirContentOfItems(childrenPaths, parentNode.path);

        /**
         * Children mapped by node path for easy indexing.
         * @type {Record<string, DirContent[]|undefined>}
         */
        const childrenByPath: Record<string, DirContent[] | undefined> = {};

        children.forEach(child => {
            childrenByPath[child.path] = child.children;
        });

        // Setting the children in the main treeContent object.
        parentNode.children.forEach((node) => {
            const nodeChildren = childrenByPath[node.path] as DirContent[]|undefined;

            if (!nodeChildren || !nodeChildren.length) return;

            set(node, 'children', childrenByPath[node.path]);
        });
    }

    /**
     * Changes the node text by changing the innerText of it's element.
     * @param {string} nodeKey
     * @param {string} newName
     */
    const renameNode = (nodeKey: string, newName: string) => {
        const nodeRef: HTMLElement | null = document.getElementById(nodeKey);
        const contentEditable = nodeRef && nodeRef.querySelector('.mn-content-editable') as HTMLElement;

        if (contentEditable) contentEditable.innerText = newName;
    }

    /**
     * Given a direction gets the adjacent node object to a given target node or to the current active node.
     * @param {'prev'|'next'} direction
     * @param {DirContent|undefined} targetNode
     * @returns {DirContent|undefined}
     */
    const getAdjacentNode = (direction: 'prev'|'next', targetNode: DirContent|undefined = undefined): DirContent|undefined => {
        targetNode = targetNode || getCurrentNode.value;

        if (!targetNode) return;

        const currentNodeSiblings = getNodeSiblings(targetNode.path);

        if (!currentNodeSiblings.length) return;

        const indexModifier = direction === 'prev' ? -1 : 1;

        let adjacentNode = undefined;

        currentNodeSiblings.some((sibling, index) => {
            const node = targetNode as DirContent;

            if (sibling.path !== node.path) return false;

            adjacentNode = currentNodeSiblings[index + indexModifier];

            return true;
        });

        return adjacentNode;
    };

    /**
     * Gets the first child of a target node.
     * @param {DirContent} targetNode
     */
    const getFirstChild = (targetNode: DirContent): DirContent|undefined => {
        const nodeChildren = targetNode.children;
        return nodeChildren && nodeChildren[0];
    }

    /**
     * Given a target node gets its first child of a specified type.
     * @param {DirContent} targetNode
     * @type {'dir'|'file'}
     * @returns {DirContent|undefined}
     */
    const getFirstOfType = (targetNode: DirContent, type: DirContent["type"]): DirContent|undefined => {
        const nodeChildren = getNodeChildren(targetNode.path);
        let firstFile: DirContent|undefined;

        if (!nodeChildren || !nodeChildren.length) return;

        nodeChildren.some((node) => {
            if (node.type !== type) return false;

            firstFile = node;

            return true;
        });

        return firstFile;
    }

    /**
     * Given a target node gets its first child of a type 'file'.
     * @param {DirContent} targetNode
     * @returns {DirContent|undefined}
     */
    const getFirstFile = (targetNode: DirContent): DirContent|undefined => {
        return getFirstOfType(targetNode, 'file');
    }

    /**
     * Gets the previous node object to a given target node or to the current active node.
     * @param {DirContent|undefined} targetNode
     * @returns {DirContent|undefined}
     */
    const getPrevNode = (targetNode: DirContent|undefined = undefined): DirContent|undefined => {
        return getAdjacentNode('prev', targetNode);
    }

    /**
     * Gets the next node object to a given target node or to the current active node.
     * @param {DirContent|undefined} targetNode
     * @returns {DirContent|undefined}
     */
    const getNextNode = (targetNode: DirContent|undefined = undefined): DirContent|undefined => {
        return getAdjacentNode('next', targetNode);
    }

    /**
     * Retrieves the current node parent, which could also be the root node.
     * If the target node is the root element returns the same target/root node.
     * @param {DirContent} targetNode
     * @returns {DirContent}
     */
    const getParentNode = (targetNode: DirContent) => {
        const fileTrail = buildDirTrailFromDirString(dirname(targetNode.path));
        const parentNode = useTreeView().getNodeFromPath(fileTrail) as DirContent;

        return parentNode || useTreeView().getRoot.value
    }

    /**
     * Given a target node, sets the document focus on the adjacent node towards a given direction.
     * @param {'prev'|'next'} direction
     * @param {DirContent} targetNode
     */
    const focusOnAdjacentNode = (direction: 'prev'|'next', targetNode: DirContent) => {
        const adjacentNode = getAdjacentNode(direction, targetNode);

        if (!adjacentNode) return;

        const adjacentElement = useTreeView().getElementFromNode(adjacentNode);

        if (adjacentElement) adjacentElement.focus();
    }

    /**
     * Given a target node, sets the document focus on the previous node.
     * @param {DirContent} targetNode
     */
    const focusOnPrevNode = (targetNode: DirContent) => {
        focusOnAdjacentNode('prev', targetNode);
    }

    /**
     * Given a target node, sets the document focus on the next node.
     * @param {DirContent} targetNode
     */
    const focusOnNextNode = (targetNode: DirContent) => {
        focusOnAdjacentNode('next', targetNode);
    }

    /**
     * Given a direction sets active the adjacent node object to a given target node or to the current active node.
     * @param {'prev'|'next'} direction
     * @param {DirContent|undefined} targetNode
     * @returns {DirContent|undefined}
     */
    const selectAdjacentNode = (direction: 'prev'|'next', targetNode: DirContent|undefined = undefined): DirContent[] => {
        const adjacentNode = getAdjacentNode(direction, targetNode);
        const activeItems = adjacentNode ? [adjacentNode] : [];

        setActiveItems(activeItems);

        return activeItems;
    }

    /**
     * Set active the previous target node to a given node or to the currently active node.
     * @param {DirContent|undefined} targetNode
     * @returns {DirContent|undefined}
     */
    const selectPrevNode = (targetNode: DirContent|undefined = undefined): DirContent[] => {
        return selectAdjacentNode('prev', targetNode);
    }

    /**
     * Set active the next target node to a given node or to the currently active node.
     * @param {DirContent|undefined} targetNode
     * @returns {DirContent|undefined}
     */
    const selectNextNode = (targetNode: DirContent|undefined = undefined): DirContent[] => {
        return selectAdjacentNode('next', targetNode);
    }

    /**
     * Set active the first child of type 'file' of a given target node.
     * @param {DirContent} targetNode
     * @return {DirContent[]} - The new set of active nodes or undefined
     */
    const selectFirstFile = (targetNode: DirContent): DirContent[] => {
        const firstFile = getFirstFile(targetNode);
        const selectedItems: DirContent[] = [];

        if (firstFile) selectedItems.push(firstFile);

        setActiveItems(selectedItems);

        return selectedItems;
    }

    /**
     * Gets the label HTMLElement of a node and make it editable.
     * @param {DirContent} targetNode
     */
    const makeNodeEditable = (targetNode: DirContent) => {
        const targetElement = getElementFromNode(targetNode);
        const contentEditableEl = targetElement && targetElement.querySelector('.omm-content-editable') as HTMLElement;

        if (!targetElement || !contentEditableEl) return;

        makeElementSelectable(contentEditableEl);
    }

    /**
     * Adds a child to a node directory in the treeview.
     * @param {DirContent|undefined} targetNode
     * @param {DirContent} newNode
     * @returns {Promise<void>}
     */
    const addChildToNode = async (targetNode: DirContent|undefined = undefined, newNode: DirContent) => {
        if (targetNode && targetNode.type !== 'dir') return;

        let targetNodeChildren;

        if (targetNode) {
            targetNodeChildren = targetNode.children;
        } else {
            targetNodeChildren = useTreeView().getTreeContents.value as DirContent[];
        }

        if (targetNodeChildren) {
            targetNodeChildren.push(newNode);
            targetNodeChildren.sort((a, b) => sortDirContents(a, b));
        } else {
            targetNodeChildren = [newNode];
        }

        targetNode && set(targetNode, 'children', targetNodeChildren);

        targetNode && useTreeView().openItem(targetNode);

        await nextTick();

        useTreeView().setActiveItems([newNode]);

        makeNodeEditable(newNode);
    }

    /**
     * Builds a node object only given a path to the node.
     * @param nodePath
     * @param type
     * @param children
     */
    const buildNode = (nodePath: string, type: 'dir'|'file', children: DirContent[]|undefined = undefined): DirContent => {
        return {
            name: basename(nodePath),
            path: nodePath,
            treeViewKey: uuidv4(),
            type,
            children,
            dirname: dirname(nodePath),
            extension: extname(nodePath),
            new: true
        }
    }

    return {
        getTreeContents,
        getRoot,
        getCurrentNodeSiblings,
        getLastFocusedItem,
        isNodeOpen,
        getOpenItems,
        openItem,
        closeItem,
        getActiveItems,
        setActiveItems,
        setTreeContents,
        setRootChildren,
        updateTreeContents,
        getNodeFromPath,
        getFirstChild,
        loadNodesChildren,
        setRoot,
        renameNode,
        selectPrevNode,
        selectNextNode,
        focusOnPrevNode,
        focusOnNextNode,
        selectFirstFile,
        getPrevNode,
        getNextNode,
        getParentNode,
        getElementFromNodeKey,
        getElementFromNode,
        setLastFocusedItem,
        addChildToNode,
        buildNode,
        makeNodeEditable
    }
}
