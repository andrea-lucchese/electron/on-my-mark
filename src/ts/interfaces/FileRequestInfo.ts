import { AppStorage } from "@/ts";

export interface FileRequestInfo {
    relPath: string;
    storage: AppStorage;
    rootDir?: string;
    rootPathToFile?: string;
    isFile: boolean;
}
