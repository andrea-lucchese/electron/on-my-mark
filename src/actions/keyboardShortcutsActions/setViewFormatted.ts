import { useViews } from "@/compositions";

/**
 * Sets the current view to 'formatted'.
 */
export const setViewFormatted = () => {
    useViews().setViewType('formatted');
}
