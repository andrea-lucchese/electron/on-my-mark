import { concatPaths, readSettings, rootPrefix, writeSettings } from '@/background/utils';
import { IpcResponderController, AppStorage } from '@/ts';
import fs from 'fs';
import { shell } from 'electron';
import { getRootPath } from "@/background/utils/getRootPath";

export const deleteStorage: IpcResponderController = async (e, args) => {
    const storage = args.storage as AppStorage;
    let code = '200';
    let error;
    let data: AppStorage[] = [];
    let rootDir = '';

    try {
        const settings = readSettings();

        if (storage.type === 'absolute' && storage.path) {
            rootDir = storage.path;
        } else {
            rootDir = getRootPath(storage.root);
            rootDir = concatPaths([rootDir, storage.path || ''], rootPrefix());
        }

        if (fs.existsSync(rootDir)) await shell.moveItemToTrash(rootDir);

        settings.storage = settings.storage.filter((item) => {
            return item.path !== storage.path;
        });

        writeSettings(settings);

        data = settings.storage;

    } catch (nodeError) {
        code = '500';
        error = { message: `Could not create storage`, nodeError }
    }

    return { data, code, error }
}