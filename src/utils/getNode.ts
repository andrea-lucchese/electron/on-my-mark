import { useTreeView } from '@/compositions';
import { buildDirTrailFromDirString } from '@/utils';
import { DirContent } from '@/ts';

export const getNode = (pathToNode: string): DirContent => {
    const fileTrail = buildDirTrailFromDirString(pathToNode);
    const targetNode = useTreeView().getNodeFromPath(fileTrail) as DirContent;

    return targetNode || {
        name: '',
        path: '/',
        isRoot: true
    }
}