import { IpcRendererRequestArgs, IpcResponderController } from '@/ts';
import { trim } from 'lodash';
import { AppStorage } from '@/ts';
import { getRoot } from '@/background/utils/getRoot';
import fs from 'fs';

interface SaveFilterArgs extends IpcRendererRequestArgs {
    content: string;
    fileName: string;
}

/**
 * Saves a file to a given directory.
 * @param {IpcMainEvent} e 
 * @param {SaveFilterArgs} args
 * @returns {Boolean}
 */
export const saveFile: IpcResponderController = (e, args) => {
    const { content, fileName } = args as SaveFilterArgs;
    const storage = args.storage as AppStorage;
    const rootDir = getRoot(storage);
    let data;
    let code = '200';
    let error;

    try { 
        const fullPath = rootDir + '/' + trim(fileName, '/');

        fs.writeFileSync(fullPath, content, 'utf-8'); 

        data = { success: true, savedTo: fullPath, content  };
    } catch(e) { 
        code = '500';
        error = {
            message: `Failed to save ${fileName} to ${rootDir}`,
            nodeError: e
        };
    }

    return { data, error, code }
}
