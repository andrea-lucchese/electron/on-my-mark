import { IpcResponderController, AppStorage } from "@/ts";
import { getAppDir, getRoot, rootPrefix } from "@/background/utils";
import * as path from "path";
import { concatPaths } from "@/utils";
import * as fs from "fs";

/**
 * Looks for the scss template for the current storage/selection and returns it in plain css.
 * @param e
 * @param args
 */
export const sassRender: IpcResponderController = (e, args) => {
    // eslint-disable-next-line @typescript-eslint/no-var-requires
    const sass = require('sass');
    let data = '';
    let code = '200';
    let error = undefined;
    const template = args.template as string || 'default';
    const storage = args.storage as AppStorage;
    const storageRoot = getRoot(storage);

    let pathToStyles = '';

    if (!storage || !storageRoot) return { code, data, error }

    const root = path.dirname(storageRoot);

    // Checks storage root dir

    pathToStyles = concatPaths([root, `.${template}.scss`], rootPrefix());

    if (!fs.existsSync(pathToStyles)) {
        const pathToHome = getAppDir();

        pathToStyles = `${pathToHome}/settings/.default.scss`;
    }

    if (!fs.existsSync(pathToStyles)) {
        const pathToHome = getAppDir();
        pathToStyles = `${pathToHome}/settings/.default.scss`;
        fs.mkdirSync(pathToStyles, { recursive: true })
    }

    if (!pathToStyles) return { data, code, error }

    try {
        const renderData = sass.renderSync({file: pathToStyles});
        data = new TextDecoder().decode(renderData.css);
    } catch (nodeError) {
        code = '500';
        error = { message: `Could not convert scss to css`, nodeError }
    }

    return { data, code, error }
}
