import { DirContent, IpcResponderController, IpcResponderControllerResponse } from "@/ts";
import {deleteFileAttachments, getDirContent} from "@/background/controllers";

/**
 * Loops recursively withing a directory children and remove all file's attachments.
 * Used to remove all file attachments when deleting a directory.
 * @param {IpcMainEvent} e
 * @param {IpcRendererRequestArgs} args
 * @returns {IpcResponderControllerResponse}
 */
export const deleteDirAttachments: IpcResponderController = (e, args) => {
    const dirContent = getDirContent(e, args) as IpcResponderControllerResponse;

    const children = dirContent.data as DirContent[];

    if (!children || !children.length) return { data: [], code: '200' }

    const deletedAttachments: string[] = [];
    let code = '200';
    let error;

    try {
        children.forEach((child: DirContent) => {
            if (child.children) {
                const deleteDirResponse = deleteDirAttachments(e, { ...args, relPath: child.path }) as IpcResponderControllerResponse;
                const deletedFiles = deleteDirResponse.data as string[];
                deletedAttachments.concat(deletedFiles);
            }

            if (child.type === 'dir') return;

            const response = deleteFileAttachments(e, { ...args, relPath: child.path }) as IpcResponderControllerResponse;
            const deletedFiles = response.data as string[];

            deletedAttachments.concat(deletedFiles);
        });
    } catch (nodeError) {
        error = { message: `Could not delete all content of directory`, nodeError }
        code = '500';
    }

    return { data: deletedAttachments, code, error }
}
