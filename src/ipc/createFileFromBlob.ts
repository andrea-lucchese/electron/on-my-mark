import { useContents } from '@/compositions';
import ipcRendererRequest from '@/ipc/ipcRendererRequest';
import { CreateFileResponse } from '@/ts';

/**
 * Creates a file from a Blob Buffer and returns a promise to the newly created file's data.
 * @param {Buffer} buffer
 * @param {string} extension
 * @param {string} fileName
 * @returns {Promise<CreateFileResponse>}
 */
export const createFileFromBlob = async (buffer: Buffer, extension: string, fileName = ''): Promise<CreateFileResponse> => {
    const currentDir = useContents().getCurrentDir.value;
    const storage = useContents().getCurrentStorage.value;

    const dirName = currentDir;

    const response = await ipcRendererRequest({
        method: 'GET',
        action: 'createFileFromBlob',
        args: { storage, dirName, fileName, buffer, extension }
    });

    const data = response.data as CreateFileResponse;

    return data;
}