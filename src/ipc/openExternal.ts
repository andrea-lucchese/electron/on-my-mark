import ipcRendererRequest from "@/ipc/ipcRendererRequest";
import { useContents } from "@/compositions";

/**
 * Performs a request to the background to open a file in external system-defined app.
 * @param {string} url
 * @returns {Promise<undefined>}
 */
export const openExternal = async (url: string): Promise<undefined> => {
    const timeout = 0;
    const storage = useContents().getCurrentStorage.value;

    const response = await ipcRendererRequest({
        method: 'GET',
        action: 'openExternal',
        args: { url, storage },
        timeout
    });

    return response.data as undefined;
}
