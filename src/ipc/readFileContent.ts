import ipcRendererRequest from '@/ipc/ipcRendererRequest';
import { AppStorage } from '@/ts';

const readFileContent = async (fileName: string, storage: AppStorage, isAsset = false): Promise<string|Uint8Array> => {
    const response = await ipcRendererRequest({
        method: 'GET',
        action: 'readFileContent',
        args: { 
            fileName,
            storage,
            isAsset
        }
    });

    const fileContent = response.data as string;

    return fileContent;
}

export { readFileContent }