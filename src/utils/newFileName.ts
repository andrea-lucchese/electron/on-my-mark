import {getNodeSiblings} from "@/utils/getNodeSiblings";
import {DirContent} from "@/ts";

/**
 * Creates a new file name, making sure that it does not duplicate an existing one.
 * @returns {String}
 */
export const newFileName = () => {
    const baseFileName = 'New Note';
    const siblings = getNodeSiblings();

    if (!siblings.length) return baseFileName;

    const items = siblings.map((sibling: DirContent)=> sibling.treeViewKey);

    let fileName = baseFileName;
    let count = -1;

    do {
        count = count + 1;
        fileName = count === 0 ? baseFileName : baseFileName + ' [' + count + ']';
    } while (items.includes('/' + fileName + '.md'));

    return fileName;
}
