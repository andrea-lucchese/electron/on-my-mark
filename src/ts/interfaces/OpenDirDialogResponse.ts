export interface OpenDirDialogResponse {
    dir?: string[];
    hasChildren?: boolean;
}
