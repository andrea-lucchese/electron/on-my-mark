import { NoteAttachment } from "@/ts";

export interface NoteMeta {
    title?: string;
    created?: string;
    updated?: string;
    tags?: string[];
    vars?: Record<string, string>[];
    attachments?: NoteAttachment[];
}
