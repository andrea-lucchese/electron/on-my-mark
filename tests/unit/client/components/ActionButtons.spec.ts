import '@/../tests/setup';
import { shallowMount, Wrapper } from "@vue/test-utils";
import ActionButtons from "@/components/ActionButtons.vue";

import Vue from 'vue';
import AppButton from "@/components/AppButton.vue";
import AttachmentsMenu from "@/components/ActionButtons/AttachmentsMenu.vue";

describe("ActionButtons.vue", () => {
    let wrapper: Wrapper<Vue>

    beforeEach(() => {
        wrapper = shallowMount(ActionButtons, { stubs: { AppButton, AttachmentsMenu } });
        wrapper.setData({ isFile: true });
    });

    it("If the current selection is not a file action buttons don't load", async () => {
        // GIVEN current selection is not a file
        wrapper.setData({ isFile: false });

        // WHEN I load the component
        const actionButtons = await wrapper.find('.omm-action-buttons');

        // THEN buttons will not show
        expect(actionButtons.isVisible()).toBe(false);
    });

    it("If the current selection is a file action buttons load", async () => {
        const actionButtons = await wrapper.find('.omm-action-buttons');
        expect(actionButtons.isVisible()).toBe(true);
    });

    it("Attachments menu renders", async () => {
        const attachmentsMenu = await wrapper.find('.omm-attachments-menu');
        expect(attachmentsMenu.isVisible()).toBe(true);
    });

    it("Side-by-side button renders", async () => {
        const button = await wrapper.find('.omm-action-buttons__button--sbs');
        expect(button.isVisible()).toBe(true);
    });

    it("Markdown button renders", async () => {
        const button = await wrapper.find('.omm-action-buttons__button--markdown');
        expect(button.isVisible()).toBe(true);
    });

    it("Variables button renders", async () => {
        const button = await wrapper.find('.omm-action-buttons__button--variables');
        expect(button.isVisible()).toBe(true);
    });

    it("New note button renders", async () => {
        const button = await wrapper.find('.omm-action-buttons__button--new-note');
        expect(button.isVisible()).toBe(true);
    });

    it("Delete note button renders", async () => {
        const button = await wrapper.find('.omm-action-buttons__button--delete-note');
        expect(button.isVisible()).toBe(true);
    });

    it("Export note button renders", async () => {
        const button = await wrapper.find('.omm-action-buttons__button--export-note');
        expect(button.isVisible()).toBe(true);
    });

});
