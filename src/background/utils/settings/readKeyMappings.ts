import { getAppDir } from "@/background/utils/getAppDir";
import { concatPaths } from "@/utils";
import fs from "fs";
import { AppSettings } from "@/ts/interfaces/AppSettings";
import { rootPrefix } from '@/background/utils';

export const readKeyMappings = (): AppSettings => {
    const pathToHome = getAppDir();

    const pathToKeyMappingsFile = concatPaths([pathToHome, 'settings', 'key-mappings.json'], rootPrefix());

    const mappingsPlainText = fs.readFileSync(pathToKeyMappingsFile, 'utf8');

    return JSON.parse(mappingsPlainText);
}
