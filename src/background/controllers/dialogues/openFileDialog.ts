import { dialog } from 'electron';
import { CreatedFile, IpcResponderController, AppStorage } from "@/ts";
import { getRoot, rootPrefix } from "@/background/utils";
import { concatPaths } from "@/utils";
import { v4 as uuidv4 } from 'uuid';

import fs from "fs";
import * as path from "path";

export const openFileDialog: IpcResponderController = (e, args) => {
    const data: CreatedFile[] = [];
    let code = '200';
    let error = undefined;

    const files = dialog.showOpenDialogSync({ properties: ['openFile', 'multiSelections'] });
    const storage = args.storage as AppStorage;
    const attachmentsPath = getRoot(storage, true);

    if (!files || !storage || !attachmentsPath) return { code, data, error }

    files.forEach((file) => {
        const extension = path.extname(file);
        const originalFileName = path.basename(file);
        const fileName = uuidv4() + extension;

        try {
            fs.copyFileSync(file, concatPaths([attachmentsPath, fileName], rootPrefix() ));
            data.push({ fileName, originalFileName, extension });
        } catch (nodeError) {
            code = '500';
            error = { message: `Could not create import file`, nodeError }
        }
    });


    return { data, code, error }
}
