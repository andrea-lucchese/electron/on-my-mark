import {IpcResponderController, AppStorage} from "@/ts";
import {concatPaths, getRoot, rootPrefix} from "@/background/utils";
import fs from "fs";

/**
 * Given an attachment file name deletes a single attachment.
 * @param {IpcMainEvent} e
 * @param {IpcRendererRequestArgs} args
 * @param {string} args.attachmentFileName
 */
export const deleteAttachment: IpcResponderController = (e, args) => {
    const storage = args.storage as AppStorage;
    const rootDir = getRoot(storage, true) || '';
    const attachmentFileName = args.attachmentFileName as string;

    if (!rootDir) return { code: '400', error: { message: "Rood directory is not defined" } }
    if (!attachmentFileName) return { code: '400', error: { message: "Attachment file name is not defined" } }

    const pathToFile = concatPaths([rootDir || '', attachmentFileName], rootPrefix());
    let code = '200';
    let data;
    let error;

    try {
        fs.unlinkSync(pathToFile);
        data = { deletedFile: pathToFile }
    } catch (nodeError) {
        error = { message: `Could not delete file "${pathToFile}"`, nodeError }
        code = '500';
    }

    return { data, code, error }
}
