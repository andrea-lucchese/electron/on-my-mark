import { useTreeView } from '@/compositions';
import { buildDirTrailFromDirString, dirname    } from '@/utils';
import { DirContent } from '@/ts';

export const getNodeParent = (pathToNode: string): DirContent => {
    const fileTrail = buildDirTrailFromDirString(dirname(pathToNode));
    const targetNode = useTreeView().getNodeFromPath(fileTrail) as DirContent;

    return targetNode || useTreeView().getRoot.value
}
