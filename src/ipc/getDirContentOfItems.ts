import { useContents } from '@/compositions';
import ipcRendererRequest from '@/ipc/ipcRendererRequest';
import { DirContent } from '@/ts';

const getDirContentOfItems = async (items: string[], relPath = '', reloadChildren = false): Promise<DirContent[]> => {
    const storage = useContents().getCurrentStorage.value
    const response = await ipcRendererRequest({
        method: 'GET',
        action: 'getDirContentOfDirItems',
        args: { 
            items,
            relPath,
            storage,
            reloadChildren
        }
    });
    
    const dirContent = response.data as DirContent[];
    return dirContent;
}

export { getDirContentOfItems }