import { IpcResponderController } from '@/ts';
import { defineControllerDefaults, readKeyMappings } from "@/background/utils";

/**
 * Gets the key mappings values from settings/key-mappings.json.
 * @param {IpcMainEvent} e
 * @param {IpcRendererRequestArgs} args
 */
export const getKeyMappings: IpcResponderController = () => {
    const response = defineControllerDefaults();

    try {
        const keyMappings = readKeyMappings();
        response.data = keyMappings;
    } catch (nodeError) {
        response.code = '500';
        response.error = { message: 'Could nor read key mappings file.', nodeError }
    }

    return response;
}
