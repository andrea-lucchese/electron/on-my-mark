import { NoteMeta } from "@/ts";

/**
 * Retrieves the meta information from note content.
 * @param {string} fileContent
 * @returns {NoteMeta|null} The meta object, an empty object if no meta info is found,
 * or null if fails to parse the meta string.
 */
export const getMetaFromContent = (fileContent: string): NoteMeta|null => {
    const regex = /---(.*?)---/s;
    const match = fileContent.match(regex);
    const metaString = match && match[1];
    let meta = {};

    if (!metaString) return {};

    try {
        meta = JSON.parse(metaString);
    } catch (e) {
        return null;
    }

    return meta;
}
