import { newFileName } from "@/utils/newFileName";
import { createFile } from "@/ipc";
import { useObservers } from "../compositions/useObservers";

/**
 * Creates a new file and notifies observers.
 */
export const createNewFile = async (parentDir: string|undefined = undefined) => {
    const fileName = newFileName();
    const data = await createFile(fileName, parentDir);

    await useObservers().notifyObservers('newFileCreated', {
        fileName: data.newFileName,
        relPath: data.relPathTofile
    });
}
