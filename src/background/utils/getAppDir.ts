import { app } from "electron";
import { concatPaths } from "@/utils";
import { APP_NAME } from "@/contents";
import { rootPrefix } from "./rootPrefix";

export const getAppDir = () => {
    const pathToHome = app.getPath('home');
    return concatPaths([pathToHome, APP_NAME], rootPrefix());
}