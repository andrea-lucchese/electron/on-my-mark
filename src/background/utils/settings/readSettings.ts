import { getAppDir } from "@/background/utils/getAppDir";
import { concatPaths } from "@/utils";
import fs from "fs";
import { AppSettings } from "@/ts/interfaces/AppSettings";

export const readSettings = (): AppSettings => {
    const pathToHome = getAppDir();

    const pathToSettingsFile = concatPaths([pathToHome, 'settings', 'settings.json']);

    const settingsPlainText = fs.readFileSync(pathToSettingsFile, 'utf8');
    return JSON.parse(settingsPlainText);
}
