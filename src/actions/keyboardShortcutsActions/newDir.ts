import { useContents } from "@/compositions";
import { createNewDir } from "@/utils";

export const newDir = async () => {
    const currentDir = useContents().getCurrentDir.value

    await createNewDir(currentDir);
}
