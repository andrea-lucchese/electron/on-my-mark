import { AppAction } from "@/ts";

export const clearPreviouslyOpenItems: AppAction = (args) => {
    const clearItems = args.clearItems as () => void;
    clearItems();

    return true;
}
