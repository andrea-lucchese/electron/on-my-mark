import { createDir } from "@/ipc";
import { useObservers } from "../compositions/useObservers";

/**
 * Creates a new file and notifies observers.
 */
export const createNewDir = async (parentDir: string|undefined = undefined) => {
    const response = await createDir(parentDir);

    await useObservers().notifyObservers("newDirCreated", {
        dirName: response.newDirName,
        relPath: response.relPathToDir,
        isDir: true,
    });
}
