import { useTreeView } from "@/compositions";

/**
 * Sets the focus to the last focused element of the tree or the first child of the tree
 * if no previous focused element was found.
 */
export const focusOnFirstTreeNode = () => {
    const allNodes = useTreeView().getTreeContents.value;
    const firstNode = useTreeView().getFirstChild(allNodes);
    const firstNodeElement = firstNode && useTreeView().getElementFromNode(firstNode);

    firstNodeElement && firstNodeElement.focus();
}
