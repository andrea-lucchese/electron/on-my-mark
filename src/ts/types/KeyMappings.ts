import { AppContext, KeyMapping } from "@/ts";

export type KeyMappings = Record<AppContext, KeyMapping|undefined>
