import { trim } from 'lodash';

export const buildDirTrailFromDirString = (dirName: string, trimItems = false): string[] => {
    const dirPieces = trim(dirName, '/').split('/');
    const dirTrail: string[] = [];

    dirPieces.forEach((dirName, index) => {
        const prevDirName = index > 0 ? dirTrail[index -1] : '';
        let newItem = prevDirName + '/' + dirName;

        if (trimItems) newItem = trim(newItem, '/');

        dirTrail.push(newItem);
    });

    return dirTrail;
}