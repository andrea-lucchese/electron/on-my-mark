# vue-electron-app

## Project setup
```
npm install
```

### Compiles and hot-reloads for development
```
npm run serve
```

### Electron app compiles and hot-reloads for development
```
npm run electron:serve
```

### Compiles and minifies for production
```
npm run build
```

### Electron app compiles and minifies for production
```
npm run build:electron
```

### Run your unit tests
```
npm run test:unit
```

### Lints and fixes files
```
npm run lint
```

### Customize configuration
See [Configuration Reference](https://cli.vuejs.org/config/).
