export interface AppStorage {
    /** An arbitrary name for the storage. */
    name: string;

    /** If the storage is relative to a system path or absolute. */
    type: 'relative'|'absolute'|'absolute:disabled'|'relative:disabled';

    /** The root name if it's a relative path (eg. user_home) */
    root: string;

    /** Full path to the storage, relative to the root. */
    path?: string;

    /** If it's the default storage. */
    default?: boolean;
}
