import { AppAction } from "@/ts";
import { useTreeView } from "@/compositions";
import { getNode } from "@/utils";

export const navigateTreeUp: AppAction = (args) => {
    const { getParentNode, getPrevNode, getElementFromNode, focusOnPrevNode } = useTreeView();
    const activeElement = args.activeElement as HTMLElement;
    const path = activeElement.getAttribute('data-path') as string;
    const treeNode = getNode(path);
    const prevNode = getPrevNode(treeNode);

    if (!prevNode) {
        const parentNode = getParentNode(treeNode);
        const parentElement = getElementFromNode(parentNode);
        parentElement && parentElement.focus();
    } else {
        focusOnPrevNode(treeNode);
    }

    return true;
}
