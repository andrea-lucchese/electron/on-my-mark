export type AppContext =
    'App'|
    'App/TreeView' |
    'App/TreeView/ItemLabel' |
    'App/TreeView/ItemLabel/Dir' |
    'App/TreeView/ItemLabel/File' |
    'App/NoteContent' |
    'App/NoteContent/MarkdownEditor';
