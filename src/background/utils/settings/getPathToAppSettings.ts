import { getAppDir } from "@/background/utils";

/**
 * Gets the path to a settings file in the application folder.
 * @param {string} settingFileName - The setting file name without extension, if not specified uses settings.json
 * @returns {string} the full path to the settings file.
 */
export const getPathToAppSettings = (settingFileName: string|undefined = undefined): string => {
    const pathToHome = getAppDir();

    settingFileName = settingFileName ? `${settingFileName}.json` : 'settings.json';

    return `${pathToHome}/settings/${settingFileName}`;
}