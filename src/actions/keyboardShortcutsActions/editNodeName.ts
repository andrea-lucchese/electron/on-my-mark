import { AppAction } from "@/ts";
import { makeElementSelectable } from "@/utils";

/**
 * Makes a specified node editable and selects its contents.
 * @param {Record<string, unknown>} args
 */
export const editNodeName: AppAction = (args) => {
    const event = args.event as KeyboardEvent;
    event.preventDefault();

    const nodeLabelElement = args.activeElement as HTMLElement;

    const contentEditableElement = nodeLabelElement.querySelector('.omm-content-editable') as HTMLElement;

    makeElementSelectable(contentEditableElement);

    return true;
}
