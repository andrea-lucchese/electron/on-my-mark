import { IpcResponderController } from "@/ts";
import { defineControllerDefaults, saveFileDialog } from "@/background/utils";
import { convertToPdf } from "@/background/utils/convertToPdf";

export const exportToPdf: IpcResponderController = async (e, args) => {
    const response = defineControllerDefaults();
    const rawHtml = args.rawHtml as string;
    let pathToFile: string|undefined = '';

    try {
        pathToFile = saveFileDialog({
            title: 'Select or create an export file',
            filters: [{ name: 'PDF files', extensions: ['pdf'] }]
        });

        if (!pathToFile) {
            return response;
        }

        await convertToPdf(rawHtml, pathToFile);

    } catch (nodeError) {
        response.code = '500';
        response.error = { message: 'Could not export file.', nodeError }
    }

    response.data = pathToFile;

    return response;
}
