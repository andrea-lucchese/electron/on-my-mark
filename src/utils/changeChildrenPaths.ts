import { DirContent } from "@/ts";
import { concatPaths, getNodeChildren } from "@/utils";
import { set } from "@vue/composition-api";

/**
 * Given a target node changes the children's path to a new given path to the node.
 * Used when changing a directory name to update the treeview. The targetNode must be a reference to
 * an object contained in the treeContents global property.
 * @param targetNode The node of which we want to rename the paths of the children.
 * @param newPath The new path
 * @param oldPath The old path
 */
export const changeChildrenPaths = (targetNode: DirContent, newPath: string, oldPath: string) => {
    const targetNodeChildren = getNodeChildren(oldPath);

    if (!targetNodeChildren) return;

    const newChildren = [ ...targetNodeChildren ];

    targetNodeChildren.forEach((child: DirContent, index) => {
        const oldChildPath = child.path;
        const childPathWithoutRoot = child.path.slice(oldPath.length + 1, child.path.length);
        const changedPath = concatPaths([newPath, childPathWithoutRoot], '/');

        if (child.children && child.children.length) {
            changeChildrenPaths(child, changedPath, oldChildPath);
        }

        newChildren[index].path = changedPath;
    });

    set(targetNode, 'children', newChildren);
}