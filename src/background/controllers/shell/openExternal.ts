import { IpcResponderController, AppStorage } from "@/ts";
import { shell } from 'electron';

import { getRoot, rootPrefix } from "@/background/utils";
import { concatPaths } from "@/utils";
import * as path from "path";

/**
 * Open link in external app.
 * @todo consider a validation https://github.com/ogt/valid-url/blob/master/index.js
 * @param e
 * @param args
 */
export const openExternal: IpcResponderController = (e, args) => {
    const data = undefined;
    let code = '200';
    let error = undefined;

    const url = args.url as string;
    const storage = args.storage as AppStorage;
    const attachmentsPath = getRoot(storage, true);

    if (!storage || !attachmentsPath) return { code, data, error }

    try {
        const attachmentName = path.basename(url);
        void shell.openPath(concatPaths([attachmentsPath, attachmentName], rootPrefix()));
    } catch (nodeError) {
        code = '500';
        error = { message: 'It was not possible to open the give url externally', nodeError }
    }

    return { data, code, error }
}
