import '@/../tests/setup';
import { mockComposableMethod, mockImportedMethod } from '@/../tests/utils';
import { shallowMount, Wrapper } from "@vue/test-utils";
import { useContents } from "@/compositions";

import Vue from 'vue';
import AttachmentsMenu from "@/components/ActionButtons/AttachmentsMenu.vue";
import AppButton from "@/components/AppButton.vue";
import ContentEditable from "@/components/ContentEditable.vue";
import { RenameAttachmentResult } from "@/ts";

describe("AttachmentsMenu.vue - Items", () => {
    let wrapper: Wrapper<Vue>

    beforeEach(() => {
        wrapper = shallowMount(AttachmentsMenu,
            { stubs: { AppButton, ContentEditable } }
        );
    });

    it("Items do NOT load if there are no attachments", async () => {
        // GIVEN no attachments are loaded

        // WHEN I load the component
        const menuItems = await wrapper.findAll('.omm-attachments-menu-item--attachment');

        // THEN no menu items will show
        expect(menuItems.length).toEqual(0);
    });

    it("There are as many menu items as the number of the attachments", async () => {
        const attachments: { name: string; label: string }[] = [];

        for (let i = 0; i < 5; i++) {
            attachments.push({name: `attachment${ i }`, label: `label${ i }`});

            await wrapper.setData({attachments});

            const menuItems = await wrapper.findAll('.omm-attachments-menu-item--attachment');

            expect(menuItems.length).toEqual(i + 1);
        }
    });
});

describe("AttachmentsMenu.vue - Delete button", () => {
    let wrapper: Wrapper<Vue>

    beforeEach(async () => {
        wrapper = shallowMount(AttachmentsMenu);

        const attachments = [
            { name: 'name-1', label: 'Label 1' },
            { name: 'name-2', label: 'Label 2' }
        ]
        await wrapper.setData({ attachments });
        await wrapper.setProps({ value: true });
    });

    it("Attachments menu item buttons render.", async () => {
        const appButtons = await wrapper.findAllComponents(AppButton);

        expect(appButtons.length > 0).toBe(true);
        expect(appButtons.at(0).isVisible()).toBe(true);
    });

    it("Delete buttons render correctly", async () => {
        const deleteButtons = wrapper.findAll('.omm-attachments-menu-item__button--delete');

        expect(deleteButtons.length).toEqual(2);

        expect(deleteButtons.at(0).isVisible()).toBe(true);
        expect(deleteButtons.at(1).isVisible()).toBe(true);
    });

    it("Delete button click passes an attachment object to its handler", async () => {
        const deleteButton = wrapper.find('.omm-attachments-menu-item__button--delete');

        const handleDeleteAttachmentClick = jest.fn(() => undefined);

        await wrapper.setData({ handleDeleteAttachmentClick });

        await deleteButton.vm.$emit('click');

        expect(handleDeleteAttachmentClick).toHaveBeenCalledWith({ name: 'name-1', label: 'Label 1'});
    });

    it("If current text contains attachment name attachment is not removed", async () => {
        const deleteButton = wrapper.find('.omm-attachments-menu-item__button--delete');

        const currentText = useContents().getCurrentText();

        currentText.value = 'something >>> @attachment/name-1 <<< around the attachment';

        const saveFile = await mockImportedMethod('@/ipc/saveFile', 'saveFile');
        const deleteAttachment = await mockImportedMethod('@/ipc/deleteAttachment', 'deleteAttachment');

        await deleteButton.vm.$emit('click');

        expect(saveFile).not.toHaveBeenCalled();
        expect(deleteAttachment).not.toHaveBeenCalled();
    });

    it("When attachment is removed successfully removeMetaAttachment() and saveFile() are not called.", async () => {
        const deleteButton = wrapper.find('.omm-attachments-menu-item__button--delete');

        const currentText = useContents().getCurrentText();

        currentText.value = "Let's put back the text to something that not contains the file name.";

        const deleteAttachmentNotSuccessful = await mockImportedMethod(
            '@/ipc/deleteAttachment',
            'deleteAttachment',
            async () => { return false }
        );

        const saveFile = await mockImportedMethod('@/ipc/saveFile', 'saveFile');

        const removeMetaAttachment = jest.fn();

        await mockComposableMethod(
            '@/compositions/useContents',
            'useContents',
            'removeMetaAttachment',
            removeMetaAttachment
        );

        await deleteButton.vm.$emit('click');

        expect(deleteAttachmentNotSuccessful).toHaveBeenCalled();
        expect(removeMetaAttachment).not.toHaveBeenCalled();
        expect(saveFile).not.toHaveBeenCalled();
    });

    it("When attachment is not removed successfully removeMetaAttachment() and saveFile() are called.", async () => {
        const deleteButton = wrapper.find('.omm-attachments-menu-item__button--delete');

        const deleteAttachmentSuccessful = await mockImportedMethod(
            '@/ipc/deleteAttachment',
            'deleteAttachment',
            async () => { return 'returns a file name' }
        );

        const saveFile = await mockImportedMethod('@/ipc/saveFile', 'saveFile');

        const removeMetaAttachment = jest.fn();

        await mockComposableMethod(
            '@/compositions/useContents',
            'useContents',
            'removeMetaAttachment',
            removeMetaAttachment
        );

        await deleteButton.vm.$emit('click');

        expect(deleteAttachmentSuccessful).toHaveBeenCalled();
        expect(removeMetaAttachment).toHaveBeenCalled();
        expect(saveFile).toHaveBeenCalled();
    });
});

describe("AttachmentsMenu.vue - Item Content Edit", () => {
    let wrapper: Wrapper<Vue>

    beforeEach(async () => {
        wrapper = shallowMount(AttachmentsMenu, {
            stubs: { ContentEditable }
        });

        const attachments = [
            { name: 'name-1', label: 'Label 1' },
            { name: 'name-2', label: 'Label 2' }
        ];

        await wrapper.setData({ attachments });
        await wrapper.setProps({ value: true });
    })

    it("Each item contains one ContentEditable component", async () => {
        const contentEditable = await wrapper.findAll('.omm-content-editable');

        expect(contentEditable.length).toEqual(2);
        expect(contentEditable.at(0).isVisible()).toBe(true);
        expect(contentEditable.at(1).isVisible()).toBe(true);
    });

    it("Handler is called when content editable blurs", async () => {
        const contentEditable = await wrapper.find('.omm-content-editable');

        const handleAttachmentBlur = jest.fn(() => undefined);

        await wrapper.setData({ handleAttachmentBlur });

        await contentEditable.trigger('blur');

        expect(handleAttachmentBlur).toHaveBeenCalled();
    });

    it("renameMetaAttachment() is called when content editable blurs", async () => {
        const contentEditable = await wrapper.find('.omm-content-editable');

        const renameMetaAttachment = jest.fn();

        await mockComposableMethod(
            '@/compositions/useContents',
            'useContents',
            'renameMetaAttachment',
            renameMetaAttachment
        );

        await contentEditable.trigger('blur');

        expect(renameMetaAttachment).toHaveBeenCalled();
    });

    it("saveFile() is not called when file is not successfully renamed", async () => {
        const contentEditable = await wrapper.find('.omm-content-editable');

        const renameMetaAttachment = jest.fn(() => {
            return RenameAttachmentResult.ATTACHMENT_DOES_NOT_EXISTS;
        });

        await mockComposableMethod(
            '@/compositions/useContents',
            'useContents',
            'renameMetaAttachment',
            renameMetaAttachment
        );

        const saveFile = await mockImportedMethod('@/ipc/saveFile', 'saveFile');

        saveFile.mockClear();

        await contentEditable.trigger('blur');

        expect(saveFile).not.toHaveBeenCalled();
    });

    it("Rename button renders", async () => {
        const renameButton = await wrapper.find('.omm-attachments-menu-item__button--rename');

        expect(renameButton.exists()).toBe(true);
    });

    it("Click on rename button makes content editable", async () => {
        const renameButton = await wrapper.find('.omm-attachments-menu-item__button--rename');

        expect(wrapper.vm.$data.editableItems['name-1']).not.toBe(true);

        await  renameButton.vm.$emit('click');

        await  renameButton.vm.$nextTick();
        await  renameButton.vm.$nextTick();

        expect(wrapper.vm.$data.editableItems['name-1']).toBe(true);
    });

});