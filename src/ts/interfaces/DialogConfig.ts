import { VueConstructor } from "vue";

export interface DialogConfig {
    show: boolean;
    props: Record<string, unknown>;
    on: Record<string, unknown>;
    component?: VueConstructor;
}
