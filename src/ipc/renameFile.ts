import { useContents } from '@/compositions';
import ipcRendererRequest from '@/ipc/ipcRendererRequest';
import { CreateFileResponse } from '@/ts';

/**
 * Performs a request to the background to rename a given file.
 * @param {string} newName
 * @param {string} relPath
 * @returns {Promise<CreateFileResponse>} information of the newly created file.
 */
export const renameFile = async (newName: string, relPath: string): Promise<CreateFileResponse> => {
    const storage = useContents().getCurrentStorage.value;

    const response = await ipcRendererRequest({
        method: 'GET',
        action: 'renameFile',
        args: { storage, relPath, newName }
    });

    const data = response.data as CreateFileResponse;

    return data;
}
