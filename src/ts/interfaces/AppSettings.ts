import { AppStorage } from "@/ts";

export interface AppSettings {
    storage: AppStorage[];
    appTheme: string;
}