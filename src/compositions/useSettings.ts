import Vue from 'vue';
import VueCompositionAPI from "@vue/composition-api";

Vue.use(VueCompositionAPI);

import { computed, ref, Ref } from "@vue/composition-api";
import { AppStorage } from '@/ts';
import { getStorage as apiGetStorage } from '@/ipc/getStorage';

const storage: Ref<AppStorage[]> = ref([]);

export const useSettings = () => {
    const getStorage = computed((): AppStorage[] => {
        return storage.value;
    });

    const setStorage = (storageNewValue: AppStorage[]) => {
        storage.value = storageNewValue;
    }

    const updateStorage = async (): Promise<AppStorage[]> => {
        const storageNewValue = await apiGetStorage();
        
        setStorage(storageNewValue);

        return storage.value;
    }

    return { getStorage, setStorage, updateStorage }
}