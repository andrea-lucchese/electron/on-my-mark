/**
 * Sets the contentEditable property of a node to true and selects the text.
 * @param targetNode
 */
export const makeElementSelectable = (element: HTMLElement) => {
    const selection = window.getSelection() || new Selection();
    const range = document.createRange();

    element.contentEditable = "true";
    element.focus();
    range.selectNodeContents(element);
    selection.removeAllRanges();
    selection.addRange(range);
}
