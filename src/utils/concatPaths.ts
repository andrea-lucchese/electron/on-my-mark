import { trim } from 'lodash';

export const concatPaths = (dirs: string[], prefix = '', suffix = ''): string => {
    let targetDirName = ''
    dirs.forEach((dir) => {
        targetDirName += trim(dir, '/') + '/';
    });

    return prefix + trim(targetDirName, '/') + suffix;
}