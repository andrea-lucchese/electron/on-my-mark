import { getRoot, concatPaths, rootPrefix } from '@/background/utils';
import { IpcResponderController, AppStorage } from '@/ts';
import fs from 'fs';

export const createDir: IpcResponderController = (e, args) => {
    const dirName = args.dirName as string || '';
    const storage = args.storage as AppStorage;
    const rootDir = getRoot(storage) || '';
    const relPath: string =  concatPaths([rootDir, dirName], rootPrefix());

    let code = '200';
    let error;
    let data;

    try {
        const items: string[] = fs.readdirSync(relPath);
        const baseFileName = 'New Folder';

        let newDirName = baseFileName;
        let count = -1;

        do {
            count = count + 1;
            newDirName = (count === 0 ? baseFileName : baseFileName + ' [' + count + ']') as string;
        } while (items.includes(newDirName));

        const pathToNewDir = concatPaths([rootDir || '', dirName, newDirName], rootPrefix());
        const relPathToDir = concatPaths([dirName, newDirName], '/');

        // fs.writeFileSync(pathToNewDir, '');
        if (!fs.existsSync(pathToNewDir)){
            fs.mkdirSync(pathToNewDir);
        }

        data = { success: true, pathToNewDir, relPathToDir, newDirName }
    } catch (nodeError) {
        code = '500';
        error = { message: `Could not create a new file`, nodeError }
    }

    return { data, code, error }
}
