import { trim } from 'lodash';

export const dirname = (path: string): string => {
    const pieces = path.split('/');

    pieces.pop();

    const final = pieces.join('/');

    return trim(final, '/');
}