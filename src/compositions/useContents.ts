import Vue from 'vue';
import VueCompositionAPI from "@vue/composition-api";

Vue.use(VueCompositionAPI);

import { computed, Ref, ref, set } from "@vue/composition-api";
import { AppStorage, RenameAttachmentResult } from "@/ts";
import { v4 as uuidv4 } from 'uuid';
import { trim } from "lodash";
import { basename, buildDirContentObject, nameWithoutExtension } from "@/utils";
import { useSettings } from "@/compositions/useSettings";

const contentHash: Ref<string> = ref('');
const currentFile: Ref<string> = ref('');
const currentDir: Ref<string> = ref('');
const currentMeta: Ref<Record<string, unknown>> = ref({});
const currentText: Ref<string> = ref('');
const currentTheme: Ref<'dark'|'light'> = ref('light');
const currentStorage: Ref<AppStorage> = ref({
    name: '',
    type: 'absolute',
    root: '',
    path: '',
    default: false
} as AppStorage);

export const useContents = () => {
    /**
     * Gets the current file name including forward slash and extension.
     */
    const getCurrentFile = computed(() => {
        return currentFile.value;
    });

    /**
     * Returns the currentText reactive property as it is.
     */
    const getCurrentText = () => {
        return currentText;
    }
    
    /**
     * Gets the current file without forward slash and extension.
     */
    const getCurrentFileName = computed(() => {
        if (!currentFile.value) return '';
        const baseName = basename(currentFile.value);
        return trim(baseName, '/').replace('.md', '');
    });

    const getCurrentDir = computed(() => {
        return currentDir.value;
    });

    const getCurrentMeta = computed(() => {
        return currentMeta.value;
    });

    const getContentHash = computed(() => {
        return contentHash.value;
    });
    
    const getCurrentStorage = computed(() => {
        return currentStorage.value;
    });

    /**
     * Returns a computed reference to the default storage object.
     * @type {AppStorage}
     */
    const getDefaultStorage = computed(() => {
        let defaultStorage;

        useSettings().getStorage.value.some((storage) => {
           if (!storage.default) return false;
           defaultStorage = storage;
           return true;
        });

        return defaultStorage;
    });

    const getCurrentTheme = computed(() => {
        return currentTheme.value
    });

    const setCurrentTheme = (theme: 'dark'|'light') => {
        currentTheme.value = theme;
    }

    const isDarkMode = computed(() => {
       return currentTheme.value === 'dark';
    });

    const setCurrentFile = (fileName: string) => {
        currentFile.value = fileName;
    };

    const setCurrentDir = (relPath: string) => {
        currentDir.value = relPath;
    };

    const setCurrentMeta = (meta: Record<string, unknown>) => {
        currentMeta.value = meta;
    };

    /**
     * Adds an attachment to the file meta.
     * @param {string} name
     * @param {string|undefined} label
     */
    const addMetaAttachment = (name: string, label: string|undefined = undefined) => {
        if (!currentMeta.value.attachments) set(currentMeta.value, 'attachments', []);

        const attachments = currentMeta.value.attachments as unknown as Record<string, string|undefined>[];

        if (label) label = nameWithoutExtension(label);

        attachments.push({ name, label })
    }

    /**
     * Removes an attachment corresponding to a given attachment name.
     * @param {string} attachmentName
     */
    const removeMetaAttachment = (attachmentName: string) => {
        if (!currentMeta.value.attachments) return;

        const attachments = currentMeta.value.attachments as unknown as Record<string, string|undefined>[];

        let index = -1;

        const containsAttachment = attachments.some((attachment) => {
            index++;
            if (attachment.name !== attachmentName) return false;
            return true;
        });

        if (containsAttachment) {
            attachments.splice(index, 1);
        }
    }

    /**
     * Rename an attachment meta.
     * @param attachmentName
     * @param newLabel
     * @return {boolean|undefined}
     *   true: label has been replaces,
     *   false: label already exist,
     *   undefined: attachment not found
     */
    const renameMetaAttachment = (attachmentName: string, newLabel: string) => {
        if (!currentMeta.value.attachments) return;

        const attachments = currentMeta.value.attachments as unknown as Record<string, string|undefined>[];

        const labelExists = attachments.filter(attachment => {
            return attachment.label === newLabel;
        });

        if (labelExists.length) return RenameAttachmentResult.LABEL_ALREADY_EXISTS;

        const renamed = attachments.some((attachment) => {
            if (attachment.name !== attachmentName) return false;
            attachment.label = newLabel;
            return true;
        });

        return renamed ? RenameAttachmentResult.LABEL_RENAMED_SUCCESSFULLY : RenameAttachmentResult.ATTACHMENT_DOES_NOT_EXISTS;
    }
    
    const setCurrentStorage = (storage: AppStorage) => {
        currentStorage.value = storage;
    };

    const updateContentHash = () => {
        contentHash.value = uuidv4();
    }

    /**
     * Determines if the current selection is a directory by comparing the current file to the current directory.
     * @type {boolean}
     */
    const isDir = computed(() => {
        return currentFile.value === getCurrentDir.value;
    });

    /**
     * Determines if the current selection is a file by comparing the current file to the current directory.
     * @type {boolean}
     */
    const isFile = computed(() => {
        return (!!currentFile.value) && (currentFile.value !== currentDir.value);
    });

    /**
     * Returns 'dir' or 'file' depending on the current content type.
     * @type {'dir'|'file'}
     */
    const getCurrentNodeType = computed((): 'dir'|'file' => {
        return isDir.value ? 'dir' : 'file';
    });

    /**
     * Gets the currently selected node.
     * @returns {DirContent}
     */
    const getCurrentNodeObject = computed(() => {
        const type = getCurrentNodeType.value;
        return buildDirContentObject(currentFile.value, type);
    })

    return {
        getCurrentDir,
        getCurrentFile,
        getCurrentFileName,
        getCurrentMeta,
        getCurrentStorage,
        getCurrentTheme,
        getContentHash,
        getDefaultStorage,
        isDir,
        isFile,
        getCurrentNodeType,
        getCurrentNodeObject,
        setCurrentMeta,
        addMetaAttachment,
        setCurrentFile,
        setCurrentDir,
        setCurrentStorage,
        updateContentHash,
        getCurrentText,
        setCurrentTheme,
        removeMetaAttachment,
        renameMetaAttachment,
        isDarkMode
    }
}