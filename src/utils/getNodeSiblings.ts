import { DirContent } from '@/ts';
import { getNodeParent } from '@/utils';
import { useContents, useTreeView } from '@/compositions';

export const getNodeSiblings = (pathToNode = ''): DirContent[] => {
    pathToNode = pathToNode || useContents().getCurrentFile.value;

    const parentNode = getNodeParent(pathToNode);

    if (!parentNode.isRoot) {
        return parentNode.children as DirContent[];
    } else {
        const root = useTreeView().getRoot.value;
        return root.children as DirContent[];
    }
}