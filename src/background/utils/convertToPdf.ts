import fs from "fs";
import { BrowserWindow } from 'electron';

/**
 * Converts a given HTML string to PDF and save to a specified location.
 * @param {string} rawHtml
 * @param {string} pathToFile
 */
export const convertToPdf = async (rawHtml: string, pathToFile: string) => {
    const options = {
        printBackground: true,
        landscape: false,
        pageSize: 'A4',
        scaleFactor: 80
    };

    const win = new BrowserWindow ({
        show: false,
        webPreferences: {
            webSecurity: false
        }
    });

    await win.loadURL(`data:text/html;charset=utf-8, <html><body>${rawHtml}</body></html>`);

    win.webContents.printToPDF(options).then(data => {

        fs.writeFile(pathToFile, data, (error) => {
            if (error) throw error;
        })
    });
}
