export interface CreateFileResponse {
    /**
     * If the creation was successful.
     */
    success: boolean;

    /**
     * The absolute path to the newly created file/directory.
     */
    pathToFile: string;

    /**
     * The path to file/directory relative the current storage.
     */
    relPathTofile: string;

    /**
     * The name of the file/directory including extension (if any).
     */
    newFileName: string;
}