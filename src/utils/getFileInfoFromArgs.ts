import { IpcRendererRequestArgs, AppStorage, FileRequestInfo } from "@/ts";
import { concatPaths, getRoot } from "@/background/utils";
import fs from "fs";

/**
 * Extracts file information from a request args.
 * @param {IpcRendererRequestArgs} args
 * @returns {FileRequestInfo}
 */
export const getFileInfoFromArgs = (args: IpcRendererRequestArgs): FileRequestInfo => {
    const relPath = args.relPath as string;
    const storage = args.storage as AppStorage;
    const rootDir = getRoot(storage);

    const rootPathToFile = rootDir ? concatPaths([rootDir, relPath]) : undefined;
    const isFile = rootPathToFile ? fs.statSync(rootPathToFile).isFile() : false;

    return {relPath, storage, rootDir, rootPathToFile, isFile}
}
