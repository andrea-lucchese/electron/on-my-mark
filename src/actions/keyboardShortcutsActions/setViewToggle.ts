import { useEditor, useViews } from "@/compositions";
import { nextTick } from "@vue/composition-api";

/**
 * Toggles the current view between 'markdown' and 'formatted' .
 */
export const setViewToggle = async () => {
    const viewType = useViews().getViewType();
    const toggledView = viewType.value === 'markdown' ? 'formatted' : 'markdown';
    useViews().setViewType(toggledView);

    await nextTick();

    if (toggledView === 'markdown') {
        const editor = useEditor().getEditor.value;

        await nextTick();

        editor && editor.focus();
    }
}
