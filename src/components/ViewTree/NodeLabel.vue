<template>
    <div
        ref="nodeRef"
        :id="item.treeViewKey"
        class="mn-treeview-label"
        :class="item.new ? 'mn-treeview-label--new' : ''"
        @click="$emit('click', item, isEditable)"
        @focus="$emit('focus', item)"
        @blur="handleBlur"
        @contextmenu="openContextMenu($event, item)"
        :data-context="computeContext"
    >
        <v-tooltip bottom right open-delay="1000">
            <template v-slot:activator="{ attrs, on }">
                <span
                    v-bind="attrs"
                    v-on="handleOn(on)"
                >
                    <content-editable
                        v-model="isEditable"
                        :key="initialValue"
                        :itemKey="item.treeViewKey"
                        :initialValue="initialValue"
                        @becomeNonEditable="becomeNonEditable($event, item)"
                        @blur="handleContentEditableBlur($event, item)"
                    />

                </span>
            </template>
            {{ initialValue }}
        </v-tooltip>
    </div>
</template>

<script lang="ts">
import {
    computed,
    defineComponent,
    onBeforeMount,
    PropType,
    Ref,
    ref
} from '@vue/composition-api';
import { upperFirst } from 'lodash';
import ContentEditable from '@/components/ContentEditable.vue';
import { ContextMenuArgs, DirContent } from '@/ts';
import { useContents, useContextMenu, useObservers, useTreeView } from '@/compositions';
import { renameFile } from '@/ipc';
import { buildDirContentObject, concatPaths, dirname, nameWithoutExtension } from '@/utils';

export default defineComponent({
    name: 'NodeLabel',
    components: { ContentEditable },
    props: {
        item: {
            type: Object as PropType<DirContent>
        }
    },
    setup(props) {
        const item = props.item as DirContent;
        const nodeRef: Ref<HTMLElement | undefined> = ref();
        const itemName = computed(() => {
            return nameWithoutExtension(item.name);
        });

        const initialValue: Ref<string> = ref(itemName.value);

        const isEditable: Ref<boolean> = ref(false);

        const isRenaming = { value: false }

        /**
         * Shows the context menu.
         * @param {MouseEvent} event
         * @param {DirContent} item - The target node on which the context item is opened.
         */
        const openContextMenu = (event: MouseEvent, item: DirContent) => {
            const context = 'TreeView/item/' + item.type;
            const args = item as unknown as ContextMenuArgs;

            useContextMenu().openMenu(event, args, context);
        }

        /**
         * Initiates the renaming request by calling the renameFile ipc method.
         * @param {string} newName
         * @param {DirContent} item
         * @return {boolean} True if renaming is successful.
         */
        const performRenaming = async (newName: string, item: DirContent) => {
            const response = await renameFile(newName, item.path);

            return response.success !== false;
        }

        /**
         * Updates currentFile and currentDir of the current selection.
         * @return {Promise<void>}
         */
        const updateCurrentSelection = (newName: string, type: 'dir' | 'file' = 'file'): Promise<void> => {
            return new Promise((resolve) => {
                if (item.type === 'dir') return resolve();

                const currentDir = useContents().getCurrentDir.value;
                let newCurrentFile: string;

                if (type === 'file') {
                    newCurrentFile = concatPaths([ currentDir, newName ], '/', '.md');
                } else {
                    const parentDir = dirname(currentDir);
                    newCurrentFile = concatPaths([ parentDir, newName ], '/');

                    useContents().setCurrentDir(newCurrentFile);
                }

                useContents().setCurrentFile(newCurrentFile);

                resolve();
            });
        }

        /**
         * Notifies observers that a file name has been updated.
         * @param {string} newName
         */
        const emitUpdatedFileEvent = (newName: string): Promise<void> => {
            return new Promise((resolve) => {
                const oldPathToFile = useContents().getCurrentFile.value;
                const currentDir = useContents().getCurrentDir.value;
                const newPathToFile = concatPaths([ currentDir, newName ], '/', '.md');

                const oldNode = buildDirContentObject(oldPathToFile);
                const newNode = buildDirContentObject(newPathToFile);

                useObservers().notifyObservers('fileNameUpdated', {oldNode, newNode}).then(() => {
                    resolve();
                });
            });
        }

        /**
         * Notifies observers that a directory name has been updated.
         * @param {string} newName
         * @param {DirContent} item
         */
        const emitUpdatedDirEvent = (newName: string, item: DirContent): Promise<void> => {
            return new Promise((resolve) => {
                const oldPathToDir = item.path;
                const dirPath = dirname(oldPathToDir);
                const newPathToDir = concatPaths([ dirPath, newName ], '/');

                const oldNode = buildDirContentObject(oldPathToDir);

                const newNode = buildDirContentObject(newPathToDir);

                useObservers().notifyObservers('dirNameUpdated', {oldNode, newNode}).then(() => {
                    resolve();
                });
            });
        }

        /**
         * Show tooltip only if the mouse stays still
         * @param {Record<string, (e: MouseEvent) => void>} on
         */
        const handleOn = (on: Record<string, (e: MouseEvent) => void>) => {
            on.mousemove = on.mouseenter;
            return on;
        }

        /**
         * Tries to perform label renaming, or revert the value if unsuccessful.
         * @param {DirContent} item
         * @returns {Promise<void>}
         */
        const renameNode = async (newName: string, item: DirContent) => {
            if (isRenaming.value === true) return;

            isRenaming.value = true;

            newName = newName.replace('.md', '');

            const renamingSuccessful = await performRenaming(newName, item);

            if (!renamingSuccessful) {
                initialValue.value = props.item ? 'forceUpdate::' + itemName.value : '';
                return;
            }

            initialValue.value = newName;

            if (item.type == 'file') {
                await emitUpdatedFileEvent(newName);
            } else {
                await emitUpdatedDirEvent(newName, item);
            }

            await updateCurrentSelection(newName, item.type);

            isRenaming.value = false;
        }

        const becomeNonEditable = async (newName: string, item: DirContent) => {
            if (newName === itemName.value) return;
            await renameNode(newName, item);
        }

        const handleBlur = () => {
            useTreeView().setLastFocusedItem(item);
        }

        const handleContentEditableBlur = () => {
            nodeRef.value && nodeRef.value.focus();
        }

        const computeContext = computed(() => {
            return 'App/TreeView/ItemLabel/' + upperFirst(item.type);
        });

        onBeforeMount(() => {
            initialValue.value = props.item ? itemName.value : '';
        });

        return {
            openContextMenu,
            handleOn,
            handleContentEditableBlur,
            handleBlur,
            becomeNonEditable,
            initialValue,
            computeContext,
            isEditable,
            nodeRef
        }
    },
})
</script>

<style lang="scss">
.mn-treeview-label {
    &--new {
        color: var(--v-info-lighten1);;
    }

    &:focus {
        outline: none;
        text-decoration: underline;
    }
}
</style>
