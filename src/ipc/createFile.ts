import { useContents } from '@/compositions';
import ipcRendererRequest from '@/ipc/ipcRendererRequest';
import { CreateFileResponse } from '@/ts';

export const createFile = async (fileName = '', parentDir: string|undefined = undefined): Promise<CreateFileResponse> => {
    const dirName = parentDir || useContents().getCurrentDir.value;
    const storage = useContents().getCurrentStorage.value;

    const response = await ipcRendererRequest({
        method: 'GET',
        action: 'createFile',
        args: { storage, dirName, fileName }
    });

    const data = response.data as CreateFileResponse;

    return data;
}
