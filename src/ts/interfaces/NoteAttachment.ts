export interface NoteAttachment {
    name: string;
    label: string;
}
