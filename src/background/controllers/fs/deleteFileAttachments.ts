import fs from "fs";
import { IpcResponderController } from "@/ts";
import { getFileInfoFromArgs, getMetaFromContent } from "@/utils";
import { deleteAttachment } from "@/background/controllers";

/**
 * Deletes all attachments of a note.
 * @param {IpcMainEvent} e
 * @param {IpcRendererRequestArgs} args
 * @returns {IpcResponderControllerResponse}
 */
export const deleteFileAttachments: IpcResponderController = (e, args) => {
    const { storage, rootPathToFile } = getFileInfoFromArgs(args);

    if (!rootPathToFile) {
        return { code: '400', error: { message: "Could not retrieve path to file" } }
    }

    let code = '200';
    let error;
    const deletedAttachments: string[] = [];
    const fileContent = fs.readFileSync(rootPathToFile, 'utf8');
    const meta = getMetaFromContent(fileContent);

    try {
        if (meta && meta.attachments) {
            meta.attachments.forEach((attachment) => {
                deleteAttachment(e, {
                    action: 'removeAttachment',
                    uuid: args.uuid,
                    storage,
                    attachmentFileName: attachment.name
                });

                deletedAttachments.push(attachment.name);
            });
        }
    } catch (nodeError) {
        error = { message: `Could not delete attachments of "${rootPathToFile}"`, nodeError }
        code = '500';
    }

    return { data: deletedAttachments, code, error }
}
