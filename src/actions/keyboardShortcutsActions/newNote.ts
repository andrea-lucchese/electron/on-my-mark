import { useContents } from "@/compositions";
import { createNewFile } from "@/utils";

export const newNote = async () => {
    const currentDir = useContents().getCurrentDir.value

    await createNewFile(currentDir);
}
