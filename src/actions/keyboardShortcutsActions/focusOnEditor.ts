import { useEditor } from "@/compositions";

export const focusOnEditor = () => {
    const editor = useEditor().getEditor.value;
    editor && editor.focus();
}
