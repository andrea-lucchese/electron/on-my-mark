import { getRoot, concatPaths, rootPrefix } from '@/background/utils';
import { IpcResponderController, AppStorage } from '@/ts';
import { v4 as uuidv4 } from 'uuid';
import fs from 'fs';
import { dirname } from "@/utils";

/**
 * Creates a file in the attachments folder of a given args.storage from a Buffer.
 * @param {IpcMainEvent} e
 * @param {IpcRendererRequestArgs} args
 */
export const createFileFromBlob: IpcResponderController = (e, args) => {
    const buffer = args.buffer as Buffer;
    const dirName = '';
    const storage = args.storage as AppStorage;
    const rootDir = getRoot(storage, true) || '';
    const extension = '.' + args.extension as string;

    const fileName = args.fileName as string || uuidv4();

    const pathToFile = concatPaths([rootDir || '', dirName, fileName], rootPrefix(), extension);
    const parentDir = dirname(pathToFile);
    const relPathToFile = concatPaths([dirName, fileName], '/', extension);

    let code = '200';
    let error;
    let data;

    try {
        if (!fs.existsSync(parentDir)) {
            fs.mkdirSync(parentDir, { recursive: true });
        }

        fs.writeFileSync(pathToFile, buffer);

        data = { success: true, pathToFile, relPathToFile, fileName }
    } catch (nodeError) {
        code = '500';
        error = { message: `Could not create a new file`, nodeError }
    }

    return { data, code, error }
}