import { AppStorage } from "@/ts";
import ipcRendererRequest from "@/ipc/ipcRendererRequest";

/**
 * Performs a request to create a storage.
 * @param {AppStorage} storage - The storage to be created.
 * @returns {AppStorage[]} - The new settings storage array.
 */
export const createStorage = async (storage: AppStorage) => {
    const response = await ipcRendererRequest({
        method: 'GET',
        action: 'createStorage',
        args: {  storage }
    });

    const data = response.data as {
        storage: AppStorage[];
        newStorage: AppStorage;
    };

    return data;
}
