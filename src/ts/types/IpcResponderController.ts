import { IpcMainEvent } from 'electron/main';
import { IpcRendererRequestArgs, IpcResponderControllerResponse } from '@/ts';

export type IpcResponderController = (event: IpcMainEvent, args: IpcRendererRequestArgs) => IpcResponderControllerResponse|Promise<IpcResponderControllerResponse>;
