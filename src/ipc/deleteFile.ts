import { useContents } from '@/compositions';
import ipcRendererRequest from '@/ipc/ipcRendererRequest';
import { CreateFileResponse } from '@/ts';

export const deleteFile = async (relPath = ''): Promise<CreateFileResponse> => {
    const storage = useContents().getCurrentStorage.value;
    relPath = relPath || useContents().getCurrentFile.value;
    const response = await ipcRendererRequest({
        method: 'GET',
        action: 'deleteFile',
        args: { storage, relPath }
    });

    const data = response.data as CreateFileResponse;

    return data;
}