import { useContents } from '@/compositions';
import ipcRendererRequest from '@/ipc/ipcRendererRequest';
import { CreateDirResponse } from '@/ts';

export const createDir = async (dirName = ''): Promise<CreateDirResponse> => {
    dirName = dirName || useContents().getCurrentDir.value;
    const storage = useContents().getCurrentStorage.value;

    const response = await ipcRendererRequest({
        method: 'GET',
        action: 'createDir',
        args: { storage, dirName }
    });

    const data = response.data as CreateDirResponse;

    return data;
}