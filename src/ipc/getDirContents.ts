import { useContents } from '@/compositions';
import ipcRendererRequest from '@/ipc/ipcRendererRequest';

export const getDirContents = async () => {
    const storage = useContents().getCurrentStorage.value;
    const relPath = useContents().getCurrentDir.value

    const response = await ipcRendererRequest({ 
        method: 'GET', 
        action: 'getDirContent',
        args: { storage, relPath }
    });

    return response && response.data;
}