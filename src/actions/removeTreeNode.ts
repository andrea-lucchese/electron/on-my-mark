import { set } from '@vue/composition-api';
import { useContents, useTreeView } from '@/compositions';
import { AppAction, DirContent } from '@/ts';
import { getNodeParent, getNodeChildren } from '@/utils';

/**
 * Removes a file from the tree.
 * @param {Record<deletedItem, DirContent>} args 
 * @returns {boolean}
 */
export const removeTreeNode: AppAction = (args) => {
    const item = args.deletedItem as DirContent;
    const parentNode = getNodeParent(item.path);
    const parentNodeChildren = getNodeChildren(parentNode.path);
    const activeItem = useContents().getCurrentNodeObject.value;

    // If we are deleting the currently selected item move the selection to the previous node
    // (if available) or the next node.
    if (activeItem.path === item.path) {
        const selectedNodes = useTreeView().selectPrevNode();
        if (!selectedNodes.length) useTreeView().selectNextNode();
    }

    if (!parentNodeChildren) return true;
    
    const updatedParentNodeChildren = parentNodeChildren.filter((child: DirContent) => {
        return child.path !== item.path;
    });

    if (!parentNode.isRoot) {
        set(parentNode, 'children', updatedParentNodeChildren);
    } else {
        useTreeView().setTreeContents(updatedParentNodeChildren);
    }

    return true;
}