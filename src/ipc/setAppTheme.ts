import ipcRendererRequest from '@/ipc/ipcRendererRequest';

/**
 * Requests to change the appTheme settings parameter.
 */
export const setAppTheme = async (appTheme: 'dark'|'light'): Promise<'dark'|'light'> => {
    const response = await ipcRendererRequest({
        method: 'GET',
        action: 'setAppTheme',
        args: { appTheme }
    });

    return response.data as 'dark'|'light';
}
