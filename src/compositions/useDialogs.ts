import Vue, { VueConstructor } from 'vue';
import VueCompositionAPI, { ref, Ref, set } from "@vue/composition-api";

Vue.use(VueCompositionAPI);

import { DialogName, DialogConfig } from "@/ts";

interface ActivateDialogBindings {
    props?: Record<string, unknown>;
    on?: Record<string, unknown>;
}

const appDialogs: Ref<Record<DialogName, DialogConfig>> = ref({
    createStorage: { show: false, props: {}, on: {} },
    deleteStorage: { show: false, props: {}, on: {} }
});

export const useDialogs = () => {
    /**
     * Returns an editable reference to the appDialogs object.
     * @type {Ref<Record<DialogName, DialogConfig>>}
     */
    const getDialogs = () => {
        return appDialogs;
    };

    /**
     * Given a dialog name checks if a dialogue is active on inactive by returning the show property.
     * @param {string} dialogName
     * @returns {boolean}
     */
    const getDialogStatus = (dialogName: DialogName) => {
        return appDialogs.value[dialogName].show;
    }

    /**
     * Shows a dialog identified by name and assigns props and listeners to the dialog component.
     * @param {string} dialogName
     * @param {VueConstructor} component
     * @param {ActivateDialogBindings} bindings
     */
    const activateDialog = (dialogName: DialogName, component: VueConstructor, bindings: ActivateDialogBindings = {}) => {
        set(appDialogs.value[dialogName], 'component', component);
        set(appDialogs.value[dialogName], 'props', bindings.props || {});
        set(appDialogs.value[dialogName], 'on', bindings.on || {});

        appDialogs.value[dialogName].show = true;
    }

    /**
     * Hides a dialog identified by name and reset all props and listeners.
     * @param {string} dialogName
     */
    const deActivateDialog = (dialogName: DialogName) => {
        appDialogs.value[dialogName].show = false;
        appDialogs.value[dialogName].props = {}
    }

    return { getDialogs, getDialogStatus, activateDialog, deActivateDialog }
}
