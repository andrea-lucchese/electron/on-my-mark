import { DirContent } from '@/ts';
import { getNode } from '@/utils';
import { useTreeView } from '@/compositions';

/**
 * Given a path to node finds the children in the treeview content.
 * @todo IMPORTANT: the method should return undefined or null if the child is not found.
 * @param pathToNode
 */
export const getNodeChildren = (pathToNode: string): DirContent[]|undefined => {
    const targetNode = getNode(pathToNode);

    if (!targetNode.isRoot) {
        return targetNode.children as DirContent[]|undefined;
    } else {
        return useTreeView().getTreeContents.value as DirContent[]|undefined;
    }
}