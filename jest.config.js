module.exports = {
  preset: "@vue/cli-plugin-unit-jest/presets/typescript-and-babel",
    collectCoverageFrom: [
        'src/**/*.{ts,js,vue}',
        '!src/main.ts', // No need to cover bootstrap file
    ]
};
