import fs from 'fs';
import { DirContent, IpcResponderController } from '@/ts';

export const sortItems: IpcResponderController = (e, args) => {
    const defaults = {
        dirsFirst: true,
        sortBy: 'name',
        sort: 'ASC'
    }

    args = { ...defaults, ...args }

    const items = args.items as string[];
    const relPath = args.relPath as string;
    let error;
    let code = '200';

    if (!items.length) return { data: [], code };

    const rootPath = __dirname + '/' + relPath;
    const dirs: DirContent[] = [];
    const files: DirContent[] = [];

    try {
        items.forEach((item) => {
            const rootPathToItem = rootPath + '/' + item;
            const relPathToItem = relPath + '/' + item;
            const isDir = fs.statSync(rootPathToItem).isDirectory();
    
            const dirItem: DirContent = {
                name: item,
                path: relPathToItem,
                treeViewKey: relPathToItem,
                type: isDir ? 'dir' : 'file'
            }
    
            if (isDir) dirs.push(dirItem);
            else files.push(dirItem);
        });
    } catch (nodeError) {
        code = '500';
        error = { message: 'Cound not sort items', nodeError }
    }

    return { data: [ ...dirs, ...files ], error, code }
}