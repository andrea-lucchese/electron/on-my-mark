export interface KeyMappingConfig {
    ctrlOrCommandKey?: boolean;
    altKey?: boolean;
    shiftKey?: boolean;
    ctrlKey?: boolean;
    metaKey?: boolean;
    action: string;
}
