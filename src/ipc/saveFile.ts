import { useContents } from '@/compositions';
import ipcRendererRequest from '@/ipc/ipcRendererRequest';

interface SaveFileResponse { 
    success: boolean;
    savedTo: string;
    content: string; 
}

/**
 * Retrieve the settings storage.
 */
export const saveFile = async (content = '', fileName = ''): Promise<SaveFileResponse> => {
    const storage = useContents().getCurrentStorage.value;
    const meta = useContents().getCurrentMeta.value;

    content = content || useContents().getCurrentText().value;
    fileName = fileName || useContents().getCurrentFile.value;

    content = ('---\n' + (meta ? JSON.stringify(meta) : '{}') + '\n---\n') + content;

    const response = await ipcRendererRequest({
        method: 'GET', 
        action: 'saveFile', 
        args: { storage, content, fileName }
    });

    return response.data as SaveFileResponse;
}
