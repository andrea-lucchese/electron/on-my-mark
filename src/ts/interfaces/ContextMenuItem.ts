import { ContextMenuAction } from "../types/ContextMenuAction";

export interface ContextMenuItem {
    action: ContextMenuAction;
    title: string;
    context: string;
    name: string;
}