export const fileExtension = (fileName = '') => {
    if (!fileName) return null;
    if (!fileName.includes('.')) return null;

    return fileName.split('.').pop();
}