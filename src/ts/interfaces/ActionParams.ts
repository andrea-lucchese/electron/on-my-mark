export interface ActionParams {
    actionName: string;
    args?: Record<string, unknown>;
    priority?: number;
}