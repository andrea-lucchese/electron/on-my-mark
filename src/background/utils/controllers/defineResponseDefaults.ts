import { IpcResponderControllerResponse } from "@/ts";

export const defineControllerDefaults = (): IpcResponderControllerResponse => {
    const code = '200';
    const error: { message: string }|undefined  = undefined;
    const data: unknown = undefined;

    return { code, error, data }
}
