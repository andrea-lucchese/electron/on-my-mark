import { getRoot, concatPaths, rootPrefix } from '@/background/utils';
import { IpcResponderController, AppStorage } from '@/ts';
import fs from 'fs';

export const createFile: IpcResponderController = (e, args) => {
    const dirName = args.dirName as string || '';
    const storage = args.storage as AppStorage;
    const rootDir = getRoot(storage) || '';
    const relPath: string =  concatPaths([rootDir, dirName], rootPrefix());
    const extension = '.' + (args.extension as string || 'md');
    let code = '200';
    let error;
    let data;

    try {
        const items: string[] = fs.readdirSync(relPath);
        const baseFileName: string = args.fileName as string || 'New Note';

        let fileName: string = baseFileName;
        let count = -1;

        do {
            count = count + 1;
            fileName = (count === 0 ? baseFileName : baseFileName + ' [' + count + ']') as string;
        } while (items.includes(fileName + extension));

        const pathToFile = concatPaths([rootDir || '', dirName, fileName], rootPrefix(), extension);
        const relPathTofile = concatPaths([dirName, fileName], '/', extension)
        const newFileName = fileName + extension;

        fs.writeFileSync(pathToFile, '');

        data = { success: true, pathToFile, relPathTofile, newFileName }
    } catch (nodeError) {
        code = '500';
        error = { message: `Could not create a new file`, nodeError }
    }

    return { data, code, error }
}