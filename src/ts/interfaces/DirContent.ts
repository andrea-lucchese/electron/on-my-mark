export interface DirContent {
    /**
     * Node name and extension (if any): fileName.md or dirName, should not include path to node.
     * @todo make sure all instances do not include path to node.
     */
    name: string; 

    /** Full Path to node including extension (if any) excluding storage root: `/Root/Child/fileName.md` or `/Root/Child/dirName`   */
    path: string;

    /** Unique key to identify the treeview item, it's set to the same value of `path` but it should not change during render */
    treeViewKey: string;
    
    /** File or directory */
    type?: 'file'|'dir';

    /** Is it the storage root dir? */
    isRoot?: boolean;

    /** Node children */
    children?: DirContent[];

    /** If the node has been just created */
    new?: boolean;

    // # Properties to develop in the future
    /** Dirname of the node (everything before the last slash) */
    dirname?: string;

    /** Extension of the node (everything before the last dot) */
    extension?: string;
}
