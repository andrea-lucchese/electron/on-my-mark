import { AppStorage } from '@/ts';
import { concatPaths } from "@/utils";
import { getRootPath } from "@/background/utils/getRootPath";
import { rootPrefix } from './rootPrefix';

export const getRoot = (storage: AppStorage, isAsset = false) => {
    const mainDir = isAsset ? '/assets' : '/storage';

    if (storage.type === 'absolute') {
        return storage.path && concatPaths([storage.path, mainDir], rootPrefix());
    }

    const relPath = (storage.path ? `/${storage.path}` : '') + mainDir;

    const rootPath = getRootPath(storage.root);

    return concatPaths([rootPath, relPath], rootPrefix());
}
