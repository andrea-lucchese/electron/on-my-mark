import { useTreeView } from '@/compositions';
import { AppAction, DirContent } from '@/ts';
import { changeChildrenPaths, getNodeParent, getNodeSiblings, nameWithoutExtension } from '@/utils';
import { set } from '@vue/composition-api';

/**
 * Updates a node name in the tree and it's children.
 * @param {Record<fileName, DirContent>} args 
 * @returns {boolean}
 */
export const updateTreeNodeName: AppAction = (args) => {
    const oldNode = args.oldNode as DirContent;
    const newNode = args.newNode as DirContent;

    if (!newNode) return true;

    changeChildrenPaths(newNode, newNode.path, oldNode.path);

    const nodeSiblings = getNodeSiblings(oldNode.path);

    nodeSiblings.some((sibling: DirContent) => {
        if(sibling.path === oldNode.path) {

            set(sibling, 'path', newNode.path);
            set(sibling, 'name', newNode.name);
            return true;
        }
    });

    const targetNode = getNodeParent(oldNode.path);

    set(targetNode, 'children', nodeSiblings);

    useTreeView().renameNode(oldNode.treeViewKey, nameWithoutExtension(newNode.name));

    return true;
}