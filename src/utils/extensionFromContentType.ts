/**
 * Given a content type string, computes the relevant file extension.
 * @param contentType
 */
export const extensionFromContentType = (contentType: string) => {
    switch (contentType){
        case 'img/png': return 'png';

        default: {
            const pieces = contentType.split('/');
            return pieces[pieces.length - 1];
        }
    }
}