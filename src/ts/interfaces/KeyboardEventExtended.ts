export interface KeyboardEventExtended extends KeyboardEvent {
    newValue: string;
    oldValue: string;
    vueComponentKey: string;
}