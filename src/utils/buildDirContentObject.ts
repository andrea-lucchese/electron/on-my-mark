import { DirContent } from "@/ts";
import { basename, fileExtension } from "@/utils";

/**
 * Given a relative path to a node returns a DirContent object.
 * @param relPathToNode Relative path to node: /RootDir/Dir/NodeName.md
 * @returns {DirContent}
 */
export const buildDirContentObject = (relPathToNode: string, type: DirContent["type"] = undefined, extension = ''): DirContent => {
    const name = basename(relPathToNode);
    const path = relPathToNode;
    extension = extension || fileExtension(relPathToNode) || '';
    type = type || (extension ? 'file' : 'dir');

    return { name, path, treeViewKey: path, type }
}