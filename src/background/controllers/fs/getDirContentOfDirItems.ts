import fs from 'fs';
import { DirContent, IpcResponderController, AppStorage } from '@/ts';
import { getRoot } from '@/background/utils/getRoot';
import { trim } from 'lodash';
import * as path from "path";

/**
 * Scans an array of file/directory names within a given relative path 
 * to determine if they are files or directories and if they have children.
 * @param {IpcMainEvent} e 
 * @param {IpcRendererRequestArgs} args
 * @param {String[]} args.items Files and directory names to scan.
 * @param {String} args.relPath The relative path to scan.
 * @returns {GetDirContentOfDirItems}
 */
export const getDirContentOfDirItems: IpcResponderController = ( e, args) => {
    const defaults = {
        dirsFirst: true,
        sortBy: 'name',
        sort: 'ASC'
    }

    args = { ...defaults, ...args }

    const storage = args.storage as AppStorage;
    const relPath = args.relPath as string || '';
    const rootPath = getRoot(storage);

    const items = args.items as string[];
    let code = '200';
    let error;

    if (!items.length) return { data: [], code };

    const contents: DirContent[] = [];

    items.forEach(item => {
        const rootPathToItem = rootPath + '/' + trim(relPath, '/') + '/' + trim(item, '/');
        const relPathToItem = relPath + '/' + trim(item, '/');

        const isDir = fs.statSync(rootPathToItem).isDirectory();

        try{
            const childrenNames = isDir ? fs.readdirSync(rootPathToItem) : [];

            const dirs: DirContent[] = [];
            const files: DirContent[] = [];
    
            childrenNames.forEach((name) => {
                name = trim(name, '/')
                const rootPathToChild = rootPathToItem + '/' + name;

                const isDir = fs.statSync(rootPathToChild).isDirectory();
                
                const type = isDir ? 'dir' : 'file';
                const childPath = relPathToItem + '/' + name;
                const child = { name, path: childPath, treeViewKey: childPath, type  } as DirContent;
                
                if (isDir) dirs.push(child);
                else if (path.extname(name) === '.md') {
                    files.push(child);
                }
            });
    
            const children = args.dirsFirst ? dirs.concat(files) : files.concat(dirs);

            const content = {
                name: item,
                path: relPathToItem,
                treeViewKey: relPathToItem,
                type: isDir ? 'dir' : 'file',
                children
            } as DirContent;
            
            contents.push(content);   
        } catch(e) {
            const error = {
                message: `Directory "${rootPath}" could not be scanned for granchildren`,
                nodeError: e
            };
            code = '500';

            return { error, code }
        }
    });


    return { data: contents, error, code };
}