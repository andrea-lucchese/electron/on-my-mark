import Vue from "vue";
import VueCompositionAPI, { computed, ComputedRef, ref, Ref } from "@vue/composition-api";

Vue.use(VueCompositionAPI);

import * as monaco from "monaco-editor";


/**
 * If the editor has been already initialised.
 * @type {boolean}
 */
const editorInitialised: Ref<boolean> = ref(false);
const monacoEditor: Ref<monaco.editor.IStandaloneCodeEditor|undefined> = ref();

export const useEditor = () => {
    const isInitialised: ComputedRef<boolean> = computed(() => {
        return editorInitialised.value;
    });

    const markAsInitialised = () => {
        editorInitialised.value = true;
    }

    const markAsNotInitialised = () => {
        editorInitialised.value = false;
    }

    const setEditor = (editor: monaco.editor.IStandaloneCodeEditor) => {
        monacoEditor.value = editor;
    }

    const focusOnEditor = () => {
        if (!monacoEditor.value) return;

        monacoEditor.value.focus();
    }

    const getEditor = computed(() => {
        return monacoEditor.value;
    });

    return {
        isInitialised,
        getEditor,
        markAsInitialised,
        markAsNotInitialised,
        setEditor,
        focusOnEditor
    }
}
