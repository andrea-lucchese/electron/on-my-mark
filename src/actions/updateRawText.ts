import { AppAction } from "@/ts";
import { useContents } from "@/compositions";

/**
 * Updates the rawText of the currently edited file.
 * @param args
 */
export const updateRawText: AppAction = (args) => {
    const rawText = args.rawText as string;
    const setEditorContent = args.setEditorContent as (t: string) => void;

    const currentText = useContents().getCurrentText();

    currentText.value = rawText;

    setEditorContent(rawText);
    return true;
}
