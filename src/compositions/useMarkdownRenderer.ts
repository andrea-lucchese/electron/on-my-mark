import { computed, ref, Ref } from "@vue/composition-api";
import MarkdownIt from "markdown-it";
import MarkdownItCheckbox from "markdown-it-checkbox";
import morphdom from 'morphdom';

/**
 * Reference to the markdown view container html element.
 * @type {Ref<HTMLElement | undefined>}
 */
const viewContainerRef: Ref<HTMLElement | undefined> = ref();

/**
 * Receive a raw markdown string from the editor and updates the markdown view element.
 * Uses MarkdownIt to convert the string to HTML and morphdom to update the dom without re-rendering everything.
 * @see https://github.com/markdown-it/markdown-it
 * @see https://www.npmjs.com/package/morphdom
 * @see https://github.com/choojs/nanomorph An alternative to morphdom (should be faster but if when benchmark results slower)
 */
export const useMarkdownRenderer = () => {

    /**
     * The new raw HTML string generated based on the raw text coming from the editor.
     * @type {string}
     */
    let newRawHtml = '';

    const getViewContainerRef = computed(() => {
        return viewContainerRef.value;
    });

    /**
     * Creates and returns an instance of the MarkdownIt constructable.
     */
    const initMarkdownIt = () => {
        const markdownIt = MarkdownIt('commonmark', {
            breaks: true,
            linkify: true,
            typographer: true
        });

        markdownIt.use(MarkdownItCheckbox);

        return markdownIt;
    }

    /**
     * Renders the markdown view element for the first time.
     * @param markdownView
     */
    const firstRender = (markdownView: HTMLElement) => {
        const innerWrapper = document.createElement('div');

        innerWrapper.innerHTML = newRawHtml;
        markdownView.append(innerWrapper);
    }

    /**
     * Converts the raw text coming from the editor, initiates all other methods.
     * @param {string} rawText
     */
    const convertRawText = async (rawText: string) => {
        const markdownIt = initMarkdownIt();
        const markdownView = document.querySelector('.markdown-view') as HTMLElement;

        newRawHtml = markdownIt.render(rawText);

        if (!markdownView.innerHTML) return firstRender(markdownView);

        if (!markdownView.firstChild) return;

        const newDom = document.createElement('div');

        newDom.innerHTML = newRawHtml;

        morphdom(markdownView.firstChild, newDom);
    }

    return { viewContainerRef, convertRawText, getViewContainerRef }
}
