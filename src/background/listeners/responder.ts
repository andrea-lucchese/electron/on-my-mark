import { ipcMain } from "electron";
import * as impotortedControllers from "../controllers";

const controllers = impotortedControllers as unknown as Record<string, Function>;

import { IpcRendererRequestArgs } from '@/ts';

interface IpcMainResponseError {
    message: string;
}

interface IpcMainResponse {
    reqArgs: IpcRendererRequestArgs;
    data?: unknown;
    error?: IpcMainResponseError;
    code: string;
}

const responder = () => {
    ipcMain.on('MAIN:GET', async (e, args) => {
        args = args[0];
        const response: IpcMainResponse = { 
            reqArgs: args,
            data: undefined,
            error: undefined,
            code: '500'
        }

        if (typeof controllers[args.action] === 'function') {
            const controllerResponse = await controllers[args.action](e, args);
            response.data = controllerResponse.data;
            response.error = controllerResponse.error;
            response.code = controllerResponse.code;
        } else {
            response.error = {
                message: 'Action not supported'
            };
        }

        e.sender.send(args.uuid, [response]);
    });
}

export { responder }