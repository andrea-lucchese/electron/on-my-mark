import ipcRendererRequest from '@/ipc/ipcRendererRequest';

/**
 * Requests to change the appTheme settings parameter.
 */
export const getAppTheme = async (): Promise<'dark'|'light'> => {
    const response = await ipcRendererRequest({
        method: 'GET',
        action: 'getAppTheme',
        args: {}
    });

    return response.data as 'dark'|'light';
}
