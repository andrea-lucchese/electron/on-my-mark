import fs from 'fs';
import { getFileInfoFromArgs } from "@/utils";
import { IpcResponderController } from '@/ts';
import { deleteDirAttachments, deleteFileAttachments } from "@/background/controllers";

export const deleteFile: IpcResponderController = (e, args) => {
    const { relPath, rootDir, rootPathToFile, isFile } = getFileInfoFromArgs(args);

    if (!rootDir || !relPath || !rootPathToFile) {
        return { code: '400', error: { message: "Rood directory is not defined" } }
    }

    let code = '200';
    let data;
    let error;

    try {
        if (!isFile) {
            deleteDirAttachments(e, args);
            fs.rmdirSync(rootPathToFile, { recursive: true });
        } else {
            deleteFileAttachments(e, args);
            fs.unlinkSync(rootPathToFile);
        }
        
        data = { deletedFile: rootPathToFile }
    } catch(nodeError) {
        error = { message: `Could not delete file "${rootPathToFile}"`, nodeError }
        code = '500';
    }

    return { data, code, error }
}