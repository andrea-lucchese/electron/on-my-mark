import ipcRendererRequest from '@/ipc/ipcRendererRequest';
import { AppStorage } from '@/ts';

/**
 * Retrive the settings storage.
 */
export const getStorage = async (): Promise<AppStorage[]> => {
    const response = await ipcRendererRequest({
        method: 'GET',
        action: 'getSettings',
        args: {  name: 'storage' }
    });

    return response.data as AppStorage[];
}