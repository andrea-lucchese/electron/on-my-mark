import { useViews } from "@/compositions";

/**
 * Sets the current view to 'side-by-side' .
 */
export const setViewSideBySide = () => {
    useViews().setViewType('side-by-side');
}
