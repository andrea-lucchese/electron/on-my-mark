import { DirContent } from "@/ts";

export const sortDirContents = (a: DirContent, b: DirContent) => {
    const onlyBIsDir = a.type !== 'dir' && b.type === 'dir';
    const onlyAIsDir = a.type === 'dir' && b.type !== 'dir';
    const areOfSameType = a.type === b.type;

    if (areOfSameType && b.new) return 1;
    if (areOfSameType && a.new) return -1;
    if (onlyBIsDir) return 1;
    if (onlyAIsDir) return -1;
    return 0;
}
