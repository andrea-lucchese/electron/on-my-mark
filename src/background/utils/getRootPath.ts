import { app } from "electron";
import { getAppDir } from "@/background/utils/getAppDir";

/**
 * Gets root path for a relative storage. Eg. user home, user desktop, user documents etc..
 * @param pathType
 */
export const getRootPath = (pathType: string) => {
    switch (pathType) {
        case "omm":
            return getAppDir();
        case "electron":
            return app.getAppPath();
        case "desktop":
            return app.getPath('desktop');
        case "documents":
            return app.getPath('documents');
        case "user_home":
        default:
            return app.getPath('home');
    }
}