export type IpcRendererRequestAction =
    'getDirContent' |
    'createFile' |
    'createFileFromBlob' |
    'createDir' |
    'getDirContentOfDirItems' |
    'saveFile' |
    'readFileContent' |
    'getSettings' |
    'deleteFile' |
    'renameFile' |
    'openFileDialog' |
    'openDirDialog' |
    'sassRender' |
    'openExternal' |
    'createStorage' |
    'deleteStorage' |
    'setAppTheme' |
    'getAppTheme' |
    'getKeyMappings' |
    'exportToPdf' |
    'deleteAttachment';
