import { getRoot, concatPaths, rootPrefix } from '@/background/utils';
import { dirname } from '@/utils';

import { IpcResponderController, AppStorage } from '@/ts';
import fs from 'fs';

export const renameDir: IpcResponderController = (e, args) => {
    const newName = args.newName as string; // New file name without extension or path
    const relPath = args.relPath as string; // Relative path to current file
    const storage = args.storage as AppStorage;
    const rootDir = getRoot(storage);

    if (!rootDir) return { code: '400', error: { message: "Rood directory is not defined" } }
    if (!relPath) return { code: '400', error: { message: "Relative path is not defined" } }
    if (!newName) return { code: '400', error: { message: "New file name is not defined" } }

    const rootPathToFile = concatPaths([rootDir, relPath], rootPrefix());
    const parentDir = dirname(rootPathToFile);
    const isFile = fs.statSync(rootPathToFile).isFile();

    const newFullName = newName + (isFile ? '.md' : '');
    const items: string[] = fs.readdirSync(parentDir);

    if (items.includes(newFullName)) return {
        code: '202',
        data: { success: false, message: "File already exists" } }

    const rootPathToNewFile = concatPaths([parentDir, newFullName], rootPrefix());

    let code = '200';
    let data;
    let error;

    try {
        fs.renameSync(rootPathToFile, rootPathToNewFile);

        data = { success: true, originalFileName: rootPathToFile, newFileName: rootPathToNewFile }
    } catch(nodeError) {
        error = { message: `Could not delete file "${rootPathToFile}"`, nodeError }
        code = '500';
    }

    return { data, code, error }
}
