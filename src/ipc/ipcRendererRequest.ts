import { ipcRenderer } from 'electron';
import { v4 as uuidv4 } from 'uuid';

import {
    IpcRendererRequestMethod, 
    IpcRendererRequestAction
} from '@/ts';
import { IpcRendererEvent } from 'electron/main';

interface IpcRendererRequestParams {
    method: IpcRendererRequestMethod; 
    action: IpcRendererRequestAction; 
    args?: Record<string, unknown>; 
    timeout?: number; 
    channel?: string;
}

interface IpcRendererRequestData {
    data: unknown;
    event: IpcRendererEvent;
    request: IpcRendererRequestParams;
}

/**
 * Given specified action and method, performs a request to the background.
 * @param {IpcRendererRequestMethod} method
 * @param {IpcRendererRequestAction} action
 * @param {Record<string, unknown>} args
 * @param {number} timeout
 * @param {string} channel
 * @returns {IpcRendererRequestParams}
 */
const ipcRendererRequest = ({ method, action, args = {}, timeout = 10000, channel = 'MAIN' }: IpcRendererRequestParams): Promise<IpcRendererRequestData> => {
    const uuid = uuidv4(); 

    channel = `${channel}:${method}`;

    ipcRenderer.send(channel, [{ action, uuid, ...args }]);

    return new Promise((resolve, reject) => {
        if (timeout) {
            setTimeout(() => {
                const timeoutError = {
                    message: `Request ${uuid} took more than ${timeout} milliseconds to respond`,
                    request: { method, action, args, timeout, channel }
                }
                reject(timeoutError);
            }, timeout);
        }

        ipcRenderer.on(uuid, (event, args) => {
            if (!['200', '201', '202'].includes(args[0].code)) {
                reject({
                    error: args[0].error,
                    request: { method, action, args, timeout, channel }
                });
            }

            clearTimeout(timeout);

            resolve({
                data: args[0].data,
                event, 
                request: { method, action, args, timeout, channel }
            });      
        });
    });
}

export default ipcRendererRequest;