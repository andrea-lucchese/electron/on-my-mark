import Vue from 'vue';
import VueCompositionAPI from "@vue/composition-api";

Vue.use(VueCompositionAPI);

import { Ref, ref, nextTick, computed } from '@vue/composition-api'
import { ContextMenuAction, ContextMenuArgs, ContextMenuItem} from '@/ts';

const showMenu: Ref<boolean> = ref(false);
const menuItems: Ref<Array<ContextMenuItem>> = ref([]);
const currentMenuItems: Ref<Array<ContextMenuItem>> = ref([]);
const x: Ref<number> = ref(0);
const y: Ref<number> = ref(0);
const actionEvent: Ref<MouseEvent|undefined> = ref();
const actionParams: Ref<ContextMenuArgs|undefined> = ref();
const currentContext: Ref<string> = ref('');

export const useContextMenu = () => {
    const registerMenuItem = (item: ContextMenuItem): void => {
        menuItems.value = menuItems.value
            .filter(menuItem => menuItem.name !== item.name);

        menuItems.value.push(item);
    }

    const registerMenuItems = (items: ContextMenuItem[]): void => {
        items.forEach(item => registerMenuItem(item))
    }

    const getShowMenu = computed(() => {
        return showMenu.value;
    });

    const getMenuItems = computed(() => {
        return menuItems.value;
    });

    const getCurrentMenuItems = computed(() => {
        return currentMenuItems.value;
    });

    const getX = computed(() => {
        return x.value;
    });

    const getY = computed(() => {
        return y.value;
    });

    const setShowMenu = (shouldShow: boolean) => {
        showMenu.value = shouldShow;
    }

    const setX = (newX: number) => {
        x.value = newX;
    }

    const setY = (newY: number) => {
        y.value = newY;
    }
    
    const openMenu = (e: MouseEvent, args: ContextMenuArgs, context = '') => {
        e.preventDefault();
        useContextMenu().setShowMenu(false);
        useContextMenu().setX(e.clientX);
        useContextMenu().setY(e.clientY);

        actionParams.value = args;
        actionEvent.value = e;
        currentContext.value = context;
        currentMenuItems.value = menuItems.value.filter((item) => {
            return context.startsWith(item.context);
        });

        nextTick (() => {
            useContextMenu().setShowMenu(true);
        });     
    }; 

    const performAction = (action: ContextMenuAction) => {
        
        const event = actionEvent.value as MouseEvent;
        const params = actionParams.value;

        action(event, params);
    }

    return { 
        registerMenuItem, 
        registerMenuItems,
        setShowMenu, 
        setX, 
        setY, 
        openMenu,
        performAction,
        getShowMenu, 
        getCurrentMenuItems,
        getMenuItems, 
        getX, 
        getY 
    }
}
