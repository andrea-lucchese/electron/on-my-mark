import '@/../tests/setup';
import Vue from "vue";
import { shallowMount, Wrapper } from "@vue/test-utils";
import ContentEditable from "@/components/ContentEditable.vue";

describe("ContentEditable.vue", () => {
    let wrapper: Wrapper<Vue>

    beforeEach(() => {
        wrapper =  shallowMount(ContentEditable, {
            propsData: { itemKey: 'test' }
        });
    });

    it("ContentEditable is visible", () => {
        expect(wrapper.isVisible()).toBe(true);
    });

    it("ContentEditable by default is not editable", () => {
        const isEditable = wrapper.attributes().contenteditable;

        expect(isEditable).toEqual('false');
    });

    it("ContentEditable becomes editable if value is set to true", async () => {
        await wrapper.setProps({ value: true });

        const isEditable = wrapper.attributes().contenteditable;

        expect(isEditable).toEqual('true');
    });

    it("ContentEditable first emits 'becomeNonEditable', then 'blur' when the element blurs", async () => {
        expect(wrapper.emitted().becomeNonEditable).toBeFalsy();

        await wrapper.trigger('blur')

        expect(wrapper.emitted().becomeNonEditable).toBeTruthy();

        await wrapper.vm.$nextTick();

        expect(wrapper.emitted().blur).toBeTruthy();
    });
})