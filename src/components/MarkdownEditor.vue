<template>
    <div class="markdown-editor" :class="computeClass">
        <div
            ref="markdownEditor"
            id="markdown_editor"
            class="markdown-editor__raw"
            @keyup="handleKeyUp"
            @paste="handlePaste"
        ></div>
    </div>
</template>

<script lang="ts">
import * as monaco from 'monaco-editor';
import { computed, ComputedRef, defineComponent, nextTick, onMounted, Ref, ref, watch } from '@vue/composition-api';
import { useObservers, useAttachments, useContents, useEditor } from '@/compositions';
import { fileExtension } from "@/utils";
import { readFileContent, saveFile } from "@/ipc";
import { NoteAttachment } from "@/ts";
import { editor, IPosition, languages, Range } from "monaco-editor";
import CompletionItem = languages.CompletionItem;
import ProviderResult = languages.ProviderResult;
import CompletionList = languages.CompletionList;

export default defineComponent({
    name: 'MarkdownEditor',
    props: {
        fullWidth: {
            type: Boolean,
            default: false
        }
    },
    setup(props, { emit }) {
        const markdownEditor: Ref<HTMLElement | undefined> = ref();
        const fileContent: Ref<string> = ref('');
        const fileName: ComputedRef<string> = useContents().getCurrentFile;
        const saveTimeout: Ref<ReturnType<typeof setTimeout>|undefined> = ref();
        const rawText: Ref<string> = useContents().getCurrentText();

        let monacoEditor: monaco.editor.IStandaloneCodeEditor;

        const emitContentUpdated = () => {
            emit('contentUpdated', rawText.value);
        }

        const initMonacoAutoComplete = () => {
            monaco.languages.registerCompletionItemProvider('markdown', {
                triggerCharacters: [ '@' ],

                provideCompletionItems: ( // context: CompletionContext
                    model: editor.ITextModel,
                    position: IPosition): ProviderResult<CompletionList> => {
                    const suggestions: CompletionItem[] = [];
                    const meta = useContents().getCurrentMeta.value;
                    const attachments = meta.attachments as unknown as NoteAttachment[] | undefined;

                    if (!attachments || !attachments.length) return {suggestions: []};

                    attachments.forEach((attachment: NoteAttachment) => {
                        suggestions.push({
                            label: `attachment/${ attachment.label }`,
                            kind: monaco.languages.CompletionItemKind.File,
                            insertText: `attachment/${ attachment.name }`,
                            range: new Range(position.lineNumber, position.column, position.lineNumber, position.column)
                        });
                    });

                    return {suggestions};
                }
            });
        }

        const initMonacoMarkdownOptions = () => {
            monaco.languages.setLanguageConfiguration('markdown', {
                comments: {
                    blockComment: [ '<!--', '-->', ]
                },
                brackets: [
                    [ '{', '}' ],
                    [ '[', ']' ],
                    [ '(', ')' ]
                ],
                autoClosingPairs: [
                    {open: '{', close: '}'},
                    {open: '[', close: ']'},
                    {open: '(', close: ')'},
                    {open: '<', close: '>', notIn: [ 'string' ]}
                ],
                surroundingPairs: [
                    {open: '"', close: '"'},
                    {open: '{', close: '}'},
                    {open: '[', close: ']'},
                    {open: '(', close: ')'},
                    {open: '<', close: '>'},
                    {open: '~', close: '~'},
                    {open: '*', close: '*'},
                    {open: '_', close: '_'},
                    {open: '~', close: '~'},
                    {open: '`', close: '`'},
                    {open: '\'', close: '\''}
                ],
                folding: {
                    markers: {
                        start: new RegExp("^\\s*<!--\\s*#?region\\b.*-->"),
                        end: new RegExp("^\\s*<!--\\s*#?endregion\\b.*-->")
                    }
                }
            });
        }

        /**
         * Removes key bindings from Monaco Editor.
         */
        const removeKeyBindings = () => {
            if (!monacoEditor) return;

            monacoEditor.addCommand(monaco.KeyMod.CtrlCmd | monaco.KeyCode.KEY_F, () => {
                return {};
            });
        }

        const createEditor = () => {
            if (!markdownEditor.value) return;
            const isDarkMode = useContents().isDarkMode.value;
            const editorTheme = isDarkMode ? 'vs-dark' : 'vs-lite';

            monacoEditor = monaco.editor.create(markdownEditor.value, {
                language: "markdown",
                lineNumbers: "off",
                readOnly: false,
                quickSuggestions: false,
                automaticLayout: true,
                minimap: {
                    enabled: false
                },
                theme: editorTheme,
                autoClosingQuotes: 'languageDefined',
                autoClosingBrackets: 'always',
                autoClosingOvertype: 'always',
                wordWrap : 'on',
                renderLineHighlight: 'none',
                roundedSelection: true,
                autoSurround: 'languageDefined',
            });

            removeKeyBindings();

            useEditor().setEditor(monacoEditor);
        }

        /**
         * Initialises the Monaco editor.
         */
        const initMonacoEditor = () => {
            if (!markdownEditor.value) return;

            createEditor();

            initMonacoAutoComplete();

            setTimeout(() => {
                initMonacoMarkdownOptions();
            }, 3000);
        }

        /**
         * Given a markdown file, strips the content of the metadata and store the current metadata.
         * @param {string} newContent - The content to be parsed.
         * @return {string} The content stripped of the meta data.
         */
        const parseMetaData = (newContent: string): string => {
            const regex = /---(.*?)---/s;
            const match = newContent.match(regex);

            if (!match) {
                useContents().setCurrentMeta({});
                return newContent;
            }

            const metaString = match && match[1];
            const meta = JSON.parse(metaString);

            // @todo Set current metadata

            useContents().setCurrentMeta(meta);

            return newContent.replace(match[0], '').replace(/^\n/, '');
        }

        /**
         * Changes the content of the editor.
         * Fires onMount and when the file selection changes.
         */
        const updateTextContent = async () => {
            const currentStorage = useContents().getCurrentStorage.value
            const extension = fileExtension(fileName.value);

            rawText.value = fileContent.value.replace(/---.*?---/gs, '');

            if (!extension) {
                fileContent.value = '';
                monacoEditor.setValue(fileContent.value);
            } else {
                const newContent = await readFileContent(fileName.value.replace('storage/', ''), currentStorage) as string;

                fileContent.value = parseMetaData(newContent);

                // @todo we should wait till the monaco editor is available
                monacoEditor.setValue(fileContent.value || '');
            }

            rawText.value = fileContent.value;

            emitContentUpdated();
        }

        const save = async () => {
            await saveFile(rawText.value, fileName.value);
        }

        const debouncedActions = () => {
            saveTimeout.value !== undefined && clearTimeout(saveTimeout.value);

            saveTimeout.value = setTimeout(() => {
                save();
                emitContentUpdated();
            }, 1000);
        }

        const handleKeyUp = () => {
            rawText.value = monacoEditor.getValue();
            debouncedActions();
        }

        /**
         * Inserts a new line at current position.
         */
        const insertLine = (text: string) => {
            const line = monacoEditor.getPosition();

            if (!line || !line.lineNumber) return;

            const range = new monaco.Range(line.lineNumber, 1, line.lineNumber, 1);
            const id = {major: 1, minor: 1};
            const op = {identifier: id, range: range, text: text, forceMoveMarkers: true};

            monacoEditor.executeEdits("my-source", [ op ]);
        }

        /**
         * Retrieves pasted files, use useAttachments composition function to
         * store files and retrieves file names and insert new lines with the
         * newly created files' names.
         * @param {ClipboardEvent} event
         */
        const getPastedFile = async (event: ClipboardEvent): Promise<undefined | void> => {
            const files = await useAttachments().retrievePastedFiles(event);

            if (!files.length) return;

            files.forEach((fileName: string) => {
                insertLine(`![](@attachment/${ fileName })`);
            });
        }

        /**
         * Handles a paste event.
         * @param e
         */
        const handlePaste = (e: ClipboardEvent) => {
            getPastedFile(e);
        }

        watch(fileName, () => {
            updateTextContent();
        });

        const computeClass = computed(() => {
            if (props.fullWidth) {
                return 'full-width'
            } else return ''
        });

        /**
         * Sets the editor content and saves the content to the file.
         * @param rawText
         */
        const setEditorContent = async (rawText: string) => {
            monacoEditor.setValue(rawText);
            await nextTick();
            await save();
        }

        onMounted(() => {
            initMonacoEditor();
            updateTextContent();
        });

        watch(useContents().isDarkMode, () => {
            const monacoTheme = useContents().isDarkMode.value ? 'vs-dark' : 'vs-lite';

            monaco.editor.setTheme(monacoTheme);
        });

        useObservers().registerObserver('updateRawText', {
            actionName: 'UPDATE_RAW_TEXT',
            args: { setEditorContent }
        });

        return {fileName, markdownEditor, handleKeyUp, handlePaste, fileContent, computeClass}
    }
});
</script>

<style lang="scss" scoped>
.markdown-editor {
    display: flex;

    &__raw {
        width: 100%;
        min-height: 400px;

        ::v-deep .monaco-editor {
            padding-top: 20px;
        }
    }

    &.full-width {
        width: 100%;
    }
}
</style>
